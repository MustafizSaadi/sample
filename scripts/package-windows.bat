@ECHO off

set DIST_DIR=dist\desktop-windows
set BUILD_DIR=build
set OUT_DIR=gallery

mkdir %DIST_DIR% && pushd %DIST_DIR%
mkdir %BUILD_DIR% %OUT_DIR%

pushd %BUILD_DIR%
%QTDIR%\bin\qmake.exe ^
	-spec win32-msvc ^
	"CONFIG += release" ^
	C:\Users\mustafizur\Documents\gallery_application\Image_Gallery.pro

%MSVCROOT%\bin\jom.exe qmake_all

pushd core
%MSVCROOT%\bin\jom.exe && popd

pushd desktop
%MSVCROOT%\bin\jom.exe && popd

popd
copy %BUILD_DIR%\core\release\core.dll %OUT_DIR%
copy %BUILD_DIR%\desktop\release\desktop.exe %OUT_DIR%
%QTDIR%\bin\windeployqt %OUT_DIR%\desktop.exe %OUT_DIR%\core.dll

popd