import QtQuick 2.0

Item {
    id: item
    //    width: 350
    //    height: 450
    property alias model: gridview.model
    signal imagePressed(index :int)
    property var theme
    property var control_color

    GridView {
        id: gridview

        anchors.fill: parent
        anchors.margins: 10

        focus: true
        clip: true

        cellWidth: 250
        cellHeight: 180

        delegate: Rectangle {
            border.color: item.control_color
            color: item.theme
            width: gridview.cellWidth-30
            height: gridview.cellHeight-30
            Image {
                id: delegate

                anchors.fill: parent
                //            anchors.margins: 5

                source: model.source
                fillMode: Image.Stretch
                cache: false

                Rectangle {
                    width: parent.width
                    height: 30
                    color: "grey"
                    opacity: 0.8
                    anchors.bottom: parent.bottom

                    Text {
                        anchors.left: parent.left
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.leftMargin: 5
                        anchors.topMargin: 5
                        wrapMode: Text.WordWrap
                        font.bold: true
                        font.pointSize: 10
                        color: "white"
                        text: model.description == "" ? "No Description": model.description
                    }
                }


                MouseArea {
                    anchors.fill: parent
                    onPressed: imagePressed(index)

                }
            }
        }
    }

}
