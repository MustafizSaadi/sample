import QtQuick 2.12
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.12
import Qt.labs.platform 1.1
import QtQuick.Window 2.12
import QtQml 2.12

Item {
    id: item

    property var name
    property var description
    property var control_color

    signal saveButtonClicked()
    signal backButtonClicked()

    Column {
        id: column
        spacing: 10
        topPadding: 10
        leftPadding: 10
        rightPadding: 10
        //        bottomPadding:


        TextField {
            id: name
            placeholderText: "Name"
            maximumLength: 150
            width: item.width - 20
            height: 40
            focus: true
            clip: true
            anchors.margins: 5
            font.pointSize: 10
            background: Rectangle {
                color: "white"
                anchors.fill: parent
                radius: 10


            }
        }


        ScrollView {
            id: view
            width: item.width - 20
            height: 150
            anchors.margins: 5

            TextArea {
                id: description
                placeholderText: "Description"
                //                    maximumLength: 300
                focus: true

                //                    anchors.fill: parent
                anchors.margins: 5
                font.pointSize: 10
                wrapMode: Text.WrapAnywhere

                background: Rectangle {
                    border.color: "black"
                    radius: 10
                    anchors.fill: parent

                }
            }
        }
    }

    RowLayout{

        anchors.bottom: parent.bottom
        spacing: 10

        Button {
            id:backButton
            text: "Cancel"
            Layout.preferredWidth:  100
            Layout.preferredHeight: 40
            font.pointSize: 8
            background: Rectangle {
                anchors.fill: backButton
                color: "lightgray"
                radius: 5
            }

            onClicked: backButtonClicked()
        }

        Button {
            id:saveButton
            text: "Save"
            Layout.preferredWidth:  100
            Layout.preferredHeight: 40
            font.pointSize: 8
            background: Rectangle {
                anchors.fill: saveButton
                color: "lightseagreen"
                radius: 5
            }

            onClicked: {
                if(name.text.length != 0 && description.text.length != 0)
                {
                    item.name = name.text
                    item.description = description.text
                    saveButtonClicked()
                }
                else {
                    messageDialog.open()
                }
            }
        }


    }

    MessageDialog {
        id: messageDialog
        title: "Error!!"
        text: "Either name or description field is empty"
        buttons: MessageDialog.Ok
    }

}
