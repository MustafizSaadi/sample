import QtQuick 2.0
import QtQuick.Controls 2.0

Item {
    id: item
    property var control_color

    signal buttonClicked()

    Button {
        id: saveButton

        anchors.fill: parent
        //    hoverEnabled: false
        background: Button_Background {
            anchors.fill: parent

            color:item.control_color == "black"? "gray": "lightgray"
            source: "./assets/black_save.png"
            text: "Save"
        }

        onClicked: buttonClicked()

    }
}
