import QtQuick 2.6
import QtQuick.Controls 2.6
import QtQuick.Layouts 1.12

Item {
    id: item
    //    width: 350
    //    height: 550

    property alias button_model: buttonview.model
    property alias image_model: imageview.model
    property var control_color
    property var color
    property var theme

    signal backButtonClicked()

    ColumnLayout {
        id: column
        //    anchors.centerIn: parent
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.leftMargin: 10
        anchors.rightMargin: 10
        anchors.topMargin: 10

        spacing: 20

        Button {
            id: backButton
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.preferredWidth: 90
            Layout.preferredHeight: 40
            hoverEnabled: false
            background: Button_Background {
                //                anchors.fill: backButton
                width: 90
                height: 40
                color:item.control_color == "black"? "gray": "lightgray"
                source: "./assets/black_back_small.png"
                text: "Back"
            }

            onClicked: backButtonClicked()
        }

//        Button {
//            id: editButton
//            Layout.fillWidth: true
//            Layout.fillHeight: true
//            Layout.preferredWidth: 90
//            Layout.preferredHeight: 40
//            hoverEnabled: false
//            background: Button_Background {
//                //                anchors.fill: detailsButton
//                width: 90
//                height: 40
//                color:item.control_color == "black"? "gray": "lightgray"
//                source: listview.enabled? "./assets/black_save.png":"./assets/black_edit.png"
//                text: listview.enabled? "Save" : "Details"
//            }
//            onClicked: listview.enabled = !listview.enabled
//        }
//    }

        Text {
    //        id: name
            text: qsTr("Appearence")
            color: control_color
            font.pointSize: 10
    //        anchors.bottom: buttonview.top
    //        anchors.horizontalCenter: parent.horizontalCenter
    //        anchors.bottomMargin: 5
        }

        ListView {
            id: imageview

            focus: true
            clip: true

            width: column.width
            height: 160

            leftMargin: 10

            orientation: Qt.Horizontal
            spacing: 15

            delegate: Rectangle {
                width: 200
                height: 160
                border.color: item.control_color
                color: {console.log(item.color); item.color}

                Image {
                    id: image
                    anchors.fill: parent
                    anchors.bottomMargin: 25
                    anchors.topMargin: 5
                    anchors.leftMargin: 5
                    anchors.rightMargin: 5

                    source: model.source
                    fillMode: Image.Stretch

                }

                Text {
                    id: name
                    text: model.text
                    color: item.control_color
                    anchors.top: image.bottom
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter
                    width: parent.width
                    height: 20
                    anchors.leftMargin: 5
                    anchors.topMargin: 5
                    //                anchors.bottomMargin: 5
                    wrapMode: Text.WrapAnywhere
                    font.bold: true
                    font.pointSize: 10
                }
            }
            }

    Text {
//        id: name
        text: qsTr("Choose Themes....")
        color: control_color
        font.pointSize: 10
//        anchors.bottom: buttonview.top
//        anchors.horizontalCenter: parent.horizontalCenter
//        anchors.bottomMargin: 5
    }

    ListView {
        id:buttonview

        width: 100
        height: 200

        focus: true
        clip: true
        leftMargin: 10
//        enabled: false

        //        anchors.topMargin: 20
        //        anchors.leftMargin: 30
//        anchors.centerIn: parent

        spacing: 5

        delegate: RadioButton {
            id: control
            text: model.text
            checked: item.theme === model.text

            contentItem: Text {
                text: control.text
                //                    font: control.font
                color: control_color
                font.pointSize: 8

                leftPadding: control.indicator.width + control.spacing + 10
            }

            onCheckedChanged: {
                if(checked == true) {
                    theme = control.text
                    console.log(control.text, item.theme)
                }

            }
        }
    }

    }

}
