import QtQuick 2.0
import QtQuick.Controls 2.0
Item {
    id:item
    //    width: 350
    //    height: 550
    property var theme
    property var control_color

    property alias model: gridview.model

    signal delegateClicked(dict: var, index: int)

    signal createAlbumClicked()
    signal backButtonClicked()
    signal detailsButtonClicked(index: int)

    Button {
        id:createAlbumButton
        width: 120
        height: 35
        hoverEnabled: false
        background: Button_Background {
            //            anchors.fill: createAlbumButton
            width: 120
            height: 35
            color:item.control_color == "black"? "gray": "lightgray"
            source: "./assets/black_create.png"
            text: "Create Album"
        }
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.top: parent.top
        anchors.topMargin: 10
        onClicked: createAlbumClicked()
    }

    Button {
        id: backButton
        width: 80
        height: 35
        hoverEnabled: false
        background: Button_Background {
            //            anchors.fill: backButton
            width: 80
            height: 35
            color:item.control_color == "black"? "gray": "lightgray"
            source: "./assets/black_back_small.png"
            text: "Back"
        }
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.leftMargin: 10
        onClicked: backButtonClicked()
    }

    GridView {
        id: gridview

        anchors.top: createAlbumButton.bottom
        anchors.topMargin: 10

        anchors.left: parent.left
        anchors.leftMargin: 10

        anchors.right: parent.right
        anchors.rightMargin: 10

        anchors.bottom: parent.bottom
        anchors.bottomMargin: 100

        focus: true
        clip: true

        cellWidth:230
        cellHeight:180

        delegate: Rectangle {
            id: delegate
            width: gridview.cellWidth - 50
            height: gridview.cellHeight - 50
            border.color: item.control_color
            color: "white"
            Rectangle {
                x: 5
                y: 5
                width: parent.width
                height: parent.height + 30
                border.color: item.control_color
                color: item.theme
                clip: true
                //            property var description: model.description

                Image {
                    id: image
                    anchors.fill: parent
                    anchors.bottomMargin: 25
                    anchors.topMargin: 5
                    anchors.leftMargin: 5
                    anchors.rightMargin: 5

                    source: model.latestImageSource
                    fillMode: Image.Stretch
                    cache: false

                }

                Text {
                    id: name
                    text: model.name
                    color: item.control_color
                    anchors.top: image.bottom
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter
                    width: delegate.width
                    height: 20
                    anchors.leftMargin: 5
                    anchors.topMargin: 5
                    //                anchors.bottomMargin: 5
                    wrapMode: Text.WrapAnywhere
                    font.bold: true
                    font.pointSize: 10
                }

                MouseArea {
                    anchors.fill: parent

                    onPressed: {
                        var dict = {
                            "name" : name.text,
                            "description": model.description
                        }

                        delegateClicked(dict, index)
                    }

                }
            }
        }

    }

}
