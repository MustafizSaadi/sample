#include <QGuiApplication>
#include <QApplication>
#include <QtQml/QQmlApplicationEngine>
#include <QQuickView>
#include <QQuickItem>
#include <QQmlContext>
#include <QtMultimedia/QCamera>
#include <QtMultimedia>
#include <QObject>
#include <QIcon>

#include "image.h"
#include "user.h"
#include "themes.h"
#include "album.h"
#include "filterbyalbumproxymodel.h"
#include "databasemanager.h"
#include "imagedatetimesortproxymodel.h"
#include "albumdatetimeproxymodel.h"
#include "indexfilterproxymodel.h"
//#include "googleauth.h"

#include "downloadthread.h"

void sendToCloud(QString eventType, QString eventDescription)
{
    QMap <QString, QVariant> mp;
    QMap <QString, QVariant> event;

    event["description"] = QVariant(eventDescription);

    mp["event_properties"] = QVariant(event);
    mp["event_type"] = QVariant(eventType);
    mp["device_id"] = QVariant(QSysInfo::machineUniqueId());
    QJsonObject json = QVariant(mp).toJsonObject();

    QJsonObject payload;
//        qDebug() << "Error in creating tables";

    QString createUrl = "https://api.amplitude.com/2/httpapi";

//        QUrl url(createUrl);
//        QUrlQuery query;
    payload["api_key"] = "acb4b0f746f70584fb864c9b2cc7af09";
    payload["events"] =  json;

    QJsonDocument doc(payload);


    QNetworkAccessManager *mgr = new QNetworkAccessManager();
    QUrl url(createUrl);

    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    QNetworkReply *reply = mgr->post(request, doc.toJson());

    QObject::connect(reply, &QNetworkReply::finished, [=] () {
        if(reply->error() == QNetworkReply::NoError){
            qDebug() << "Data sent to cloud" ;
        }
        else {

            qDebug() << reply->readAll();
        }
    });
}

void parse_data(QVariant var) {

    QVariantMap mp = var.toMap();

    sendToCloud(mp["type"].toString(), mp["description"].toString());
}



int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QApplication app(argc, argv);

    QApplication::setApplicationName("Gallery");

//    qputenv("QML_DISABLE_DISK_CACHE", "1");

    QQmlApplicationEngine engine;

    Themes themes;

    User user;

    Album album;

    AlbumDatetimeProxyModel sortalbumbydatetime;
    sortalbumbydatetime.setSourceModel(&album);

    Image image(nullptr, &album);

    FilterbyAlbumProxyModel filterbyalbum;
    filterbyalbum.setSourceModel(&image);

    ImageDatetimeSortProxyModel sortimagebydatetime;
    sortimagebydatetime.setSourceModel(&filterbyalbum);

    IndexFilterProxyModel indexfilterproxymodel;
    indexfilterproxymodel.setSourceModel(&sortimagebydatetime);

    DatabaseManager database;

//    GoogleAuth googleAuth;

    DownloadThread downloadthread(nullptr, &image);

//    QCamera camera;

//        if (QMediaDevices::videoInputs().count() > 0)
//               qDebug() << "1";
//            else
//                qDebug() << "0";

    QString deviceId = QVariant(QSysInfo::machineUniqueId()).toString();


    engine.rootContext()->setContextProperty("_image_model", &image);
    engine.rootContext()->setContextProperty("_user", QVariant::fromValue(&user));
    engine.rootContext()->setContextProperty("_themes_model", &themes);
    engine.rootContext()->setContextProperty("_album_model", &album);
    engine.rootContext()->setContextProperty("_album_names", album.album_names);
    engine.rootContext()->setContextProperty("_filterbyalbum", &filterbyalbum);
    engine.rootContext()->setContextProperty("_sortimages", &sortimagebydatetime);
    engine.rootContext()->setContextProperty("_sortalbums", &sortalbumbydatetime);
    engine.rootContext()->setContextProperty("_pageimages", &indexfilterproxymodel);
    engine.rootContext()->setContextProperty("_deviceId", QVariant::fromValue(deviceId));

    engine.load(QUrl("qrc:/Main.qml"));
//    QQuickView view;

//    view.setResizeMode(QQuickView::SizeRootObjectToView);

//    view.setSource(QUrl("qrc:/Main.qml"));
////    view.setColor("black");
//    view.show();

    auto root = engine.rootObjects()[0];

    QObject::connect(root, SIGNAL(imageUploaded(QVariant)), &image, SLOT(insert_data(QVariant)));
    QObject::connect(root, SIGNAL(dataUpdated(QVariant)), &image, SLOT(update_data(QVariant)));
    QObject::connect(root, SIGNAL(newAlbum(QVariant)), &album, SLOT(insert_album(QVariant)));
    QObject::connect(root, SIGNAL(deleteImages(QVariant)), &image, SLOT(delete_data(QVariant)));
    QObject::connect(root, SIGNAL(viewAlbumChanged(int)), &filterbyalbum, SLOT(setAlbum_id(int)));
    QObject::connect(root, SIGNAL(download(QVariant)), &downloadthread, SLOT(starter(QVariant)));
    QObject::connect(root, SIGNAL(albumUpdated(QVariant)), &album, SLOT(update_album(QVariant)));
    QObject::connect(root, SIGNAL(resizeImage(QVariant)), &image, SLOT(resize_image(QVariant)));
    QObject::connect(root, SIGNAL(cropImage(QVariant)), &image, SLOT(crop_image(QVariant)));
    QObject::connect(root, SIGNAL(brightImage(QVariant)), &image, SLOT(bright_image(QVariant)));
    QObject::connect(root, SIGNAL(sharpImage(QVariant)), &image, SLOT(sharp_image(QVariant)));
    QObject::connect(root, SIGNAL(nextClicked()), &indexfilterproxymodel, SLOT(inc_index()));
    QObject::connect(root, SIGNAL(backClicked()), &indexfilterproxymodel, SLOT(dec_index()));
    QObject::connect(&image, SIGNAL(image_inserted()), &indexfilterproxymodel, SLOT(invalidate()));
//    QObject::connect(&image, SIGNAL(image_edited()), root, SIGNAL(image_edited()));

//    QQuickView edit_image_view(QUrl("qrc:/Edit_Image.qml"));
//    QObject* edit_image_item = edit_image_view.rootObject();
//    QObject::connect(edit_image_item, SIGNAL(resizeImage(QVariant)), &image, SLOT(resize_image(QVariant)));
//    QObject::connect(root, SIGNAL(pageClicked(QVariant)), SLOT(parse_data(QVariant)));
//    QObject::connect(root, pageClicked, parse_data);
    //    QObject::connect(&googleAuth, SIGNAL(image_achieved(QVariant)), &image, SLOT(insert_data(QVariant)));
    //  &app, SLOT(parse_data(QVariant)
    app.setWindowIcon(QIcon(":/assets/qt.png"));

//    Mat img = imread("C:/Users/mustafizur/Pictures/qt.png");

//    qDebug() << img.channels();

    int ret;
    try {
    ret = app.exec();
    }
    catch(QException E){
        sendToCloud("Runtime Error", E.what());
    }

    return ret;
}
