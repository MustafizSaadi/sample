import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
//import QtQuick3D
import QtMultimedia 5.12

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Camera")

    signal newImage(path: var)

    Camera {
        id: cameraview

        imageCapture {
            id: imagecapture
            onImageCaptured: {
                // Show the preview in an Image
                if(imagecapture.capturedImagePath!= "")
                    newImage(imagecapture.capturedImagePath)
            }
            onCaptureFailed:
                console.log("failed")

        }
    }

    VideoOutput {
        source: cameraview
        focus : visible // to receive focus and capture key events when visible
        anchors.fill: parent
    }

    Button {
        id: captureButton
        text: "capture"
        width: 80
        height: 35
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter

        onClicked: {
            cameraview.imageCapture.capture();
        }
    }
}
