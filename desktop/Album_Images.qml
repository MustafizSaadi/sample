import QtQuick 2.6
import QtQuick 2.15
import QtQuick.Controls 2.6
import QtQuick.Layouts 1.12

Item {
    id: item
    //    width: 350
    //    height: 550

    property alias grid_model: image_grid.model

    property var control_color
    property var theme
    property var image_count

    signal detailsButtonClicked()
    signal backButtonClicked()
    signal imagePressed(index: int)

    RowLayout {
        id: row
        //    anchors.centerIn: parent
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.leftMargin: 10

        spacing: 10

        Button {
            id: backButton
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.preferredWidth: 90
            Layout.preferredHeight: 40
            hoverEnabled: false
            background: Button_Background {
                //                anchors.fill: backButton
                width: 90
                height: 40
                color:item.control_color == "black"? "gray": "lightgray"
                source: "./assets/black_back_small.png"
                text: "Back"
            }

            onClicked: backButtonClicked()
        }

        Button {
            id: detailsButton
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.preferredWidth: 90
            Layout.preferredHeight: 40
            hoverEnabled: false
            background: Button_Background {
                //                anchors.fill: detailsButton
                width: 90
                height: 40
                color:item.control_color == "black"? "gray": "lightgray"
                source: "./assets/information.png"
                text: "Details"
            }
            onClicked: detailsButtonClicked()
        }
    }



    Text {
        id:count
        text: image_count + " photos"
        color: control_color
        font.underline: true
        anchors.leftMargin: 20
        anchors.topMargin: 30
        anchors.top: row.bottom
        anchors.left: parent.left
        wrapMode: Text.WrapAnywhere
        font.pointSize: 10
    }


    Image_Grid {
        id: image_grid
        width: item.width
        anchors.top: count.bottom
        height: item.height - 100
        theme: item.theme
        control_color: item.control_color
        //            model: grid_model
    }



    Connections {
        target: image_grid

        ignoreUnknownSignals: true


        function onImagePressed(ind) {
            console.log("Image pressed")
            item.imagePressed(ind)
        }
    }


}
