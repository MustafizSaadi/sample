import QtQuick 2.0

Item {
    id: image

    property bool cropflag: true
    property bool pointflag: true

    property var dict

    width: dict["width"]
    height: dict["height"]

    property alias cropRectX: cropRectangle.x
    property alias cropRectY: cropRectangle.y
    property alias cropRectWidth: cropRectangle.width
    property alias cropRectHeight: cropRectangle.height

    Rectangle {
        id: cropRectangle
        //            x:0 ; y:0;
        width: image.width
        height: image.height
        color: "steelblue"
        opacity: 0.5
        //            z: -1

//        visible: false

        onXChanged: {
            if(image.cropflag) {
                point4.x = cropRectangle.x
                point2.x = cropRectangle.x + cropRectangle.width
            }
        }

        onYChanged: {
            if(image.cropflag) {
                point1.y = cropRectangle.y
                point3.y = cropRectangle.y + cropRectangle.height
                console.log("main rectangle")
            }
        }

        MouseArea{
            anchors.fill: parent
            anchors.margins: 30
            drag {
                target: cropRectangle
                axis: Drag.XandYAxis
                minimumX: 0
                maximumX: image.width - cropRectangle.width
                minimumY: 0
                maximumY: image.height - cropRectangle.height
            }
            onPressed: image.pointflag = false
            onReleased: image.pointflag = true
            propagateComposedEvents: true
        }
    }

    Rectangle{

        id: point1; width: 16; height: 16; radius: width/2;
        x: cropRectangle.x + cropRectangle.width/2
        y: 0
        z: 1
        //                anchors.top: parent.top; anchors.horizontalCenter: parent.horizontalCenter;

        color: "steelblue"; visible: cropRectangle.visible;

        onYChanged: {
            if(image.pointflag){
                cropRectangle.y = y
                cropRectangle.height = (point3.y - point1.y) > 100 ? (point3.y - point1.y) : 100
                console.log(cropRectangle.height)
            }
        }

        MouseArea {
            //                    id: mouseArea
            anchors.fill: parent
            drag {
                target: point1
                axis: Drag.YAxis
                minimumY: 0
                maximumY: cropRectangle.height/2
            }
            onPressed:  image.cropflag = false
            onReleased: image.cropflag = true
            preventStealing: true
        }
    }

    Rectangle{id: point2; width: 16; height: 16; radius: width/2;
        //                anchors.right: parent.right; anchors.verticalCenter: parent.verticalCenter;
        x: image.width
        y: cropRectangle.y + cropRectangle.height/2
        color: "steelblue"; visible: cropRectangle.visible

        onXChanged: {
            if(image.pointflag)
                cropRectangle.width = (x - cropRectangle.x) > 100 ? x - cropRectangle.x : 100
        }
        MouseArea {
            //                    id: mouseArea2
            anchors.fill: parent
            drag {
                target: point2
                axis: Drag.X
                minimumX: image.width/2
                maximumX: image.width
            }
            onPressed:  image.cropflag = false
            onReleased: image.cropflag = true
        }
    }

    Rectangle{id: point3; width: 16; height: 16; radius: width/2;
        //                anchors.bottom: parent.bottom; anchors.horizontalCenter: parent.horizontalCenter
        x: cropRectangle.x + cropRectangle.width/2
        y: image.height

        color: "steelblue"; visible: cropRectangle.visible

        onYChanged: {
            if(image.pointflag)
                cropRectangle.height = (y - cropRectangle.y) > 100 ? y - cropRectangle.y : 100
        }
        MouseArea {

            anchors.fill: parent
            drag {
                target: point3
                axis: Drag.YAxis
                minimumY: image.height/2
                maximumY: image.height
            }
            onPressed:  image.cropflag = false
            onReleased: image.cropflag = true
        }
    }

    Rectangle{id: point4; width: 16; height: 16; radius: width/2;
        //                anchors.left: parent.left; anchors.verticalCenter: parent.verticalCenter;
        x: 0
        y: cropRectangle.y + cropRectangle.height/2
        color: "steelblue"; visible: cropRectangle.visible

        onXChanged: {
            if(image.pointflag){
                cropRectangle.x = x
                cropRectangle.width = (point2.x - point4.x) > 100 ? point2.x - point4.x : 100
            }
        }

        MouseArea {
            //                    id: mouseArea2
            anchors.fill: parent
            drag {
                target: point4
                axis: Drag.X
                minimumX: 0
                maximumX: image.width/2
            }

            onPressed:  image.cropflag = false
            onReleased: image.cropflag = true
        }
    }
}
