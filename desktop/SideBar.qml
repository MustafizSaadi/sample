import QtQuick 2.15
import QtQuick.Controls 2.15



Rectangle {
    id: container

    property alias model: drawer_list.model
    ListView {
        id: drawer_list
        focus: true
        clip: true
        anchors.fill: parent
        //        model: navigation_model
        spacing: 10

        delegate: ItemDelegate {
            id:delegate
            //            focus: true
            width: drawer_list.width
            height: 30

            background: Rectangle {
                id: background
                width: container.width
                height: delegate.height
                color: container.color
            }

            //                height: contentItem.height + 5
            Image {
                id: image
                source: model.source
                width: 50
                height: parent.height
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                anchors.leftMargin: 3
                fillMode: Image.PreserveAspectFit

            }

            Text {
                width: 150
                color: "white"
                text: model.label
                anchors.left: image.right
                anchors.verticalCenter: parent.verticalCenter
                anchors.leftMargin: 20
            }

            MouseArea {
                id: mousearea
                anchors.fill: parent

                //                Rectangle {
                //                    anchors.fill: parent
                //                    color: "blue"
                //                }

                //                width: container.width
                //                height: delegate.height
                hoverEnabled: true

                //                preventStealing: true

                //                enabled: true

                onEntered: {
                    console.log(mousearea.width, mousearea.height)
                    //                    background.color = "blue"
                }

                onExited: {
                    console.log("exited")
                    /*delegate.highlighted = false*/
                }

                onClicked:{
                    //                    console.log("hello")
                    //                    drawer.close()
                    model.triggered()
                    //                    console.log(delegate.text)
                }

            }

        }
    }

}



