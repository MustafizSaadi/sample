import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.3
//import QtQuick.Controls
//import QtQuick.Controls 1.4
//import Qt.labs.calendar 1.0

Item {
    id: item

    property var control_color
    property var prev_dict
    property var index

    property alias combo_model:album_combo.model
    property alias newIndex: album_combo.currentIndex

    signal backButtonClicked()
    //    signal saveButtonClicked()

    signal dataUpdated(dict: var)
    Column {
        id: column
        spacing: 10
        topPadding: 10
        leftPadding: 5
        rightPadding: 5

        TextField {
            id: location
            placeholderText: "Add location.."
            text: prev_dict["location"]
            width: item.width - 20
            height: 40
            anchors.margins: 5
            font.pointSize: 10

            background: Rectangle {
                color: "white"
                anchors.fill: parent
                radius: 10
            }
            readOnly: true

            Button {
                //add an image of globe
                width: 80
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                id: location_button
                background: Rectangle {
                    anchors.fill: location_button
                    color:"lightgray"
                    radius: 3
                }
                visible: false
                text: "Add"
                onClicked: {
                    //                        name_change_button.visible = true
                    // Do map stuffs
                }

            }


        }
        //        }


        ScrollView {
            id: view
            width: item.width - 20
            height: 100
            anchors.margins: 5
            TextArea {
                id: description
                //                    maximumLength: 300
                focus: true
                readOnly: true
                placeholderText: "Add description.."

                background: Rectangle {
                    color: "white"
                    anchors.fill: parent
                    radius: 10
                }

                text: prev_dict["description"]
                //                    anchors.fill: parent
                anchors.margins: 5
                font.pointSize: 10
                wrapMode: Text.WordWrap



            }
        }
        //        }

//        Calendar {
////            minimumDate: new Date(2017, 0, 1)
////            maximumDate: new Date(2018, 0, 1)
//        }

        ComboBox {
            id:album_combo
            width: 150
            height: 40
            currentIndex: item.index
            displayText: "Album: " + currentText
            enabled: false
            model: item.combo_model

            background: Rectangle {
                color: "lightgray"
                anchors.fill: parent
                radius: 5
            }
        }


        Button {
            id: save_button
            width: 100
            height: 30
            text: "Save Changes"
            font.pointSize: 8

            background: Rectangle {
                anchors.fill: parent
                color: "lightseagreen"
                radius: 3
            }

            visible: false


            onClicked: {
                location_button.visible = false
                save_button.visible = false
                description.readOnly = true
                album_combo.currentIndex = item.newIndex
                album_combo.enabled = false


                var dict = {
                    "location": location.text,
                    "description": description.text,
                    "album_name": album_combo.currentText,
                    "album_id": prev_dict["album_id"],
                    "source": prev_dict["source"]
                }

                if((prev_dict["location"] != dict["location"]) || (prev_dict["description"]!= dict["description"]) || (index != newIndex))
                    dataUpdated(dict)


            }
        }

    }

    RowLayout {

        anchors.bottom: parent.bottom

        spacing: 10

        Button {
            id: backButton
            Layout.preferredWidth: 100
            Layout.preferredHeight: 40
            text: "Cancel"
            font.pointSize: 8
            background: Rectangle {
                anchors.fill: backButton
                color: "lightgray"
                radius: 5
            }
            onClicked: {
                console.log("back")
                backButtonClicked()
            }
        }

        Button {
            id: editButton
            text: "Edit"
            font.pointSize: 8
            Layout.preferredWidth: 100
            Layout.preferredHeight: 40
            background: Rectangle {
                anchors.fill: editButton
                color: "lightseagreen"
                radius: 5
            }
            onClicked: {
                description.readOnly = false
                location_button.visible = true
                album_combo.enabled = true
                save_button.visible = true

            }

        }
    }

}


