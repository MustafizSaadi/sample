import QtQuick 2.0
import QtQuick.Controls 2.12

Dialog {
    id: drive_info_pop_up
    width: 300
    height: 200
    parent: Overlay.overlay
    anchors.centerIn: parent
    title: "G-Drive Filename"
    //        visible: false

    signal download(filename: var)
    signal closed()
    closePolicy: Dialog.NoAutoClose

    modal: true

    background: Rectangle {
        width: drive_info_pop_up.width
        height: drive_info_pop_up.height
        color: "slateblue"
        clip: true
    }

    contentItem: Drive_Info {
        id: drive_view
        width: 300
        height: 200

        onBackButtonClicked: {
            closed()
            drive_info_pop_up.close()
        }
        onDownloadClicked: {
            download(filename)
            drive_info_pop_up.close()
        }

    }

}

