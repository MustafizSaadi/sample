import QtQuick 2.0
import QtQuick.Controls 2.12
Dialog {
    id: create_album_pop_up
    width: 400
    height: 400

    parent: Overlay.overlay
    anchors.centerIn: parent
    title: "Create Album"
    property var obj

    signal closed()
    signal newAlbum(dict: var)

    closePolicy: Dialog.NoAutoClose

    modal: true

    background: Rectangle {
        width: create_album_pop_up.width
        height: create_album_pop_up.height
        color: "slateblue"
        clip: true
    }

    contentItem: Input_Album_Info {
        width: 400
        height: 400

        control_color: obj["control_color"]

        onSaveButtonClicked: {
            var dict = {
                "name": name,
                "description": description
            }
            newAlbum(dict)
            console.log(name, description)
            closed()
            create_album_pop_up.close()
        }

        onBackButtonClicked: {
            closed()
            create_album_pop_up.close()
        }
    }
}
