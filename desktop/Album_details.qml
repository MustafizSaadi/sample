import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Item {
    id: item

    property var prev_dict
    property var control_color
    //    property var theme
    property var count

    signal dataUpdated(dict: var)
    signal backButtonClicked()

    Column {
        id: column
        spacing: 10
        topPadding: 10
        leftPadding: 10
        rightPadding: 10


        TextField {
            id: name
            width: item.width - 20
            height: 40
            placeholderText: "Name"
            text: prev_dict["name"]
            anchors.margins: 5
            font.pointSize: 10
            readOnly: true

            background: Rectangle {
                border.color: "white"
                anchors.fill: parent
                radius: 10

            }
        }


        ScrollView {
            id: view
            width: item.width - 20
            height: 100
            anchors.margins: 5
            TextArea {
                id: description
                //                anchors.fill: parent
                //                    maximumLength: 300
                focus: true
                readOnly: true
                placeholderText: "Add description.."
                text: prev_dict["description"]
                //                    anchors.fill: parent
                anchors.margins: 5
                font.pointSize: 10
                wrapMode: Text.WrapAnywhere

                background: Rectangle {
                    color: "white"
                    anchors.fill: parent
                    radius: 10



                }
            }
        }


        Button { //add an animation for this button
            id: save_button
            width: 100
            height: 30
            text: "Save Changes"

            background: Rectangle {
                anchors.fill: save_button
                color:"lightseagreen"
                radius: 3
            }

            visible: false


            onClicked: {
                save_button.visible = false
                description.readOnly = true

                //                console.log(description.text)

                var dict = {
                    "name": name.text,
                    "description": description.text,
                    "id": prev_dict["id"]
                }

                if(prev_dict["description"]!= dict["description"])
                    dataUpdated(dict)


            }
        }

        Text {
            text: count + " photos"
            color: control_color
            font.underline: true
            anchors.margins: 5
            wrapMode: Text.WrapAnywhere
            font.pointSize: 10
        }

    }

    RowLayout {

        //    anchors.centerIn: parent
        anchors.bottom: parent.bottom

        spacing: 10

        Button {
            id: backButton
            Layout.preferredWidth: 100
            Layout.preferredHeight: 40
            text: "Cancel"
            font.pointSize: 8
            background: Rectangle {
                anchors.fill: backButton
                color: "lightgray"
                radius: 5
            }
            onClicked: {
                console.log("back")
                backButtonClicked()
            }
        }

        Button {
            id: editButton
            text: "Edit"
            font.pointSize: 8
            Layout.preferredWidth: 100
            Layout.preferredHeight: 40
            background: Rectangle {
                anchors.fill: editButton
                color: "lightseagreen"
                radius: 5
            }
            onClicked: {
                description.readOnly = false
                save_button.visible = true

            }

        }
    }
}
