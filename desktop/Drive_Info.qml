import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

Item {
    id: item

    signal backButtonClicked()
    signal downloadClicked(filename: var)



    TextField {
        id: fileInput
        width: item.width
        height: 40
        anchors.margins: 5
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.leftMargin: 5
        anchors.rightMargin: 5
        anchors.topMargin: 20
        font.pointSize: 10
        //            color: item.control_color
        placeholderText: qsTr("Enter file name (.png, .jpg, .jpeg)")
        background: Rectangle {
            anchors.fill: parent
            color: "white"
            radius: 10

        }
    }


    RowLayout {

        anchors.bottom: parent.bottom
        spacing: 10

        Button {
            id: backButton
            Layout.preferredWidth: 100
            Layout.preferredHeight: 30
            text: "Cancel"
            background: Rectangle {
                anchors.fill: backButton
                color: "lightgray"
                radius: 5
            }
            onClicked: backButtonClicked()
        }

        Button {
            id: downloadButton
            text: "Download"
            Layout.preferredWidth: 100
            Layout.preferredHeight: 30
            background: Rectangle {
                anchors.fill: downloadButton
                color: "lightseagreen"
                radius: 5
            }
            onClicked: {
                downloadClicked(fileInput.text);
            }

        }
    }

}
