import QtQuick 2.0
import QtQuick.Controls 2.0

Item {
    id: item

    property var control_color

    signal buttonClicked()

    Button {
        id: undoButton

        anchors.fill: parent
        //    hoverEnabled: false
        background: Button_Background {
            anchors.fill: undoButton

            color:item.control_color == "black"? "gray": "lightgray"
            source: "./assets/black_undo.png"
            text: "Undo"
        }
        onClicked: buttonClicked()
        //    anchors.top: parent.top
        //    anchors.left: parent.left
        //    anchors.leftMargin: 10
    }
}
