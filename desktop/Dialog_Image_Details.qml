import QtQuick 2.0
import QtQuick.Controls 2.12
Dialog {
    id: image_details_pop_up
    parent: Overlay.overlay
    anchors.centerIn: parent
    title: "Details"

    property var obj

    property var update_dict

    signal closed()
    signal dataUpdated(dict: var)

    width: 400
    height: 400
    modal: true
    closePolicy: Dialog.NoAutoClose
    //        standardButtons: Dialog.close()

    background: Rectangle {
        width: image_details_pop_up.width
        height: image_details_pop_up.height
        color: "slateblue"
        clip: true
    }

    contentItem: Input_Image_Info {

        id: input_image_view

        width: 400
        height: 400

        control_color: obj["control_color"]
        prev_dict: obj["image_details_dict"]
        index: obj["album_index"]
        combo_model: obj["combo_model"]

        onBackButtonClicked: {
            closed()
            image_details_pop_up.close()
        }
    }

    Connections {
        target: input_image_view

        ignoreUnknownSignals: true


        function onDataUpdated(dict) {
//            console.log("Updated data", dict)
//            console.log("location", dict["location"])
//            console.log("description", dict["description"])
            //                        console.log("album_name", dict["album_name"])

            update_dict = dict

            //assuming album indices did not changed in album list

            if(input_image_view.index != input_image_view.newIndex){
                console.log("two image info indices changed")
                update_dict["index"] = input_image_view.newIndex
            }

            console.log(update_dict["source"])
            dataUpdated(update_dict)
            // try to take a signal from backend on successful data uploaded
        }
    }

}

