import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
Item {

    id: item

    property var theme
    property var control_color
    property alias model: carousel.model
    property alias index: carousel.currentIndex
    //    property alias source: image.source
    signal backButtonClicked()
    signal detailsButtonClicked(dict: var)
    signal editButtonClicked(source: var)

    Button {
        id: backButton
        width: 80
        height: 35
        hoverEnabled: false
        background: Button_Background {
            //            anchors.fill: backButton
            width: 80
            height: 35
            color:item.control_color == "black"? "gray": "lightgray"
            source: "./assets/black_back_small.png"
            text: "Back"
        }
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.leftMargin: 10
        onClicked: {
            console.log("back")
            backButtonClicked()
        }
    }

    Component{
        id: delegate
        Item {
            anchors.fill:parent
            Image {
                id: image
                anchors.fill: parent

                fillMode: Image.PreserveAspectFit
                source: model.source
                cache: false
//                asynchronous: true
                Image {
                    id: detailsButton
                    source: "./assets/information.png"
                    width: 40
                    height: 40
                    fillMode: Image.PreserveAspectFit
                    anchors.right: parent.right
                    anchors.top: parent.top
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            var dict = {
                                "location": model.location,
                                "album_id": model.album_id,
                                "source": model.source,
                                "description": model.description
                            }
                            detailsButtonClicked(dict)
                        }
                    }
                }

                Image {
                    id: editButton
                    source: "./assets/edit.png"
                    width: 40
                    height: 40
                    fillMode: Image.PreserveAspectFit
                    anchors.right: detailsButton.left
                    anchors.rightMargin: 10
                    anchors.top: parent.top
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            console.log("edit button clicked")
                            editButtonClicked(model.source)
                        }
                    }
                }

                Image {
                    id: prev
                    source: "./assets/white_back_arrow.png"
                    width: 40
                    height: 40
                    fillMode: Image.PreserveAspectFit
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.leftMargin: 5

                    MouseArea {
                        anchors.fill: parent
                        onClicked: carousel.decrementCurrentIndex()
                    }

                }

                Image {
                    id: next
                    source: "./assets/white_next_arrow.png"
                    width: 40
                    height: 40
                    fillMode: Image.PreserveAspectFit
                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.rightMargin: 5

                    MouseArea {
                        anchors.fill: parent
                        onClicked: carousel.incrementCurrentIndex()
                    }

                }
            }
        }
    }

    PathView {
        id:carousel
        anchors.top: backButton.bottom
        width: item.width
        height: item.height - 200
        //        anchors.bottom: item.bottom
        anchors.topMargin: 10
        //        anchors.bottomMargin: 50
        //        model: model
        delegate: delegate
        pathItemCount: 1
        focus:true
        interactive: false
        path: Path {
            startX: carousel.x ; startY: carousel.y + 10
            PathLine { x: -item.width; y: carousel.y }
            //            PathQuad { x: 120; y: 100 }
        }

    }

}
