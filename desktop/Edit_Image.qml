import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0

Item {

    id: item

    property var theme
    property var control_color
    property var source

    signal backButtonClicked()
    signal resizeImage(dict: var)
    signal cropImage(dict: var)
    signal brightImage(dict: var)
    signal sharpImage(dict: var)

    Button {
        id: backButton
        width: 80
        height: 35
        hoverEnabled: false
        background: Button_Background {
            //            anchors.fill: backButton
            width: 80
            height: 35
            color:item.control_color == "black"? "gray": "lightgray"
            source: "./assets/black_back_small.png"
            text: "Back"
        }
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.leftMargin: 10
        onClicked: backButtonClicked()
    }

    RowLayout {
        id: topRow
        anchors.top: parent.top
        anchors.left: backButton.right
        anchors.leftMargin: 30

        spacing: 10

        Item {
            //            Layout.fillWidth: true
            //            Layout.fillHeight: true
            Layout.preferredWidth: 90
            Layout.preferredHeight: 40

            Button {
                id: resizeButton
                //        text: "G-Drive"
                //            Layout.fillWidth: true
                //            Layout.fillHeight: true
                //            Layout.preferredWidth: 90
                //            Layout.preferredHeight: 40
                anchors.fill: parent
                hoverEnabled: false

                background: Button_Background {
                    //                id: button_background
                    width: 90
                    height: 40
                    //                anchors.fill: gdriveButton
                    color:item.control_color == "black"? "gray": "lightgray"
                    source: "./assets/black_resize.png"
                    text: "Resize"
                }
                onClicked: resizeContainer.state = "expanded"

            }

            Rectangle {
                id: resizeContainer
                width: 180
                height: 125
                radius: 10
                color: "slateblue"
                visible: false
                //                z: 2
                anchors.top: resizeButton.bottom
                anchors.horizontalCenter: resizeButton.horizontalCenter

                GridLayout {
                    columns: 2
                    //                    columnSpacing: 5
                    //                    rowSpacing: 5
                    anchors.fill: parent
                    TextField {
                        id: width
                        //                width: item.width - 20
                        //                height: 40
                        placeholderText: "Width"
                        Layout.leftMargin: 5
                        //                        Layout.fillWidth: true
                        Layout.preferredWidth: 75

                        font.pointSize: 8

                        background: Rectangle {
                            border.color: "white"
                            anchors.fill: parent
                            radius: 10

                        }
                    }

                    TextField {
                        id: height
                        //                width: item.width - 20
                        //                height: 40
                        placeholderText: "Height"

                        font.pointSize: 8
                        //                        Layout.leftMargin: 5
                        //                        Layout.fillWidth: true
                        Layout.preferredWidth: 75

                        background: Rectangle {
                            border.color: "white"
                            anchors.fill: parent
                            radius: 10

                        }
                    }

                    //                    Button {Layout.preferredWidth: 70; Layout.preferredHeight: 30; onClicked: {console.log("clicked"); resizeContainer.state = "closed"}}

                    Undo_Button {
                        id: resizeundoButton; Layout.preferredWidth: 70; Layout.preferredHeight: 30; Layout.leftMargin: 10; Layout.bottomMargin: 10; control_color: item.control_color; onButtonClicked: resizeContainer.state = "closed"
                    }

                    Save_Button {
                        id: resizesaveButton; Layout.preferredWidth: 70; Layout.preferredHeight: 30; Layout.bottomMargin: 10; control_color: item.control_color
                        onButtonClicked: {
                            console.log("clicked")
                            var dict = {
                                "width": width.text,
                                "height": height.text,
                                "source": source
                            }

                            resizeImage(dict)
                            backButtonClicked()
                        }
                    }
                }

                state: "closed"

                states: [
                    State {
                        name: "closed"
                        PropertyChanges {target: resizeContainer; visible: false}
                    },
                    State {
                        name: "expanded"
                        PropertyChanges {target: resizeContainer; visible: true}
                    }
                ]
            }
        }

        Item {
            //            Layout.fillWidth: true
            //            Layout.fillHeight: true
            Layout.preferredWidth: 90
            Layout.preferredHeight: 40

            Button {
                id: cropButton
                //        text: "G-Drive"
                //            Layout.fillWidth: true
                //            Layout.fillHeight: true
                //            Layout.preferredWidth: 90
                //            Layout.preferredHeight: 40
                anchors.fill: parent
                hoverEnabled: false

                background: Button_Background {
                    //                id: button_background
                    width: 90
                    height: 40
                    //                anchors.fill: gdriveButton
                    color:item.control_color == "black"? "gray": "lightgray"
                    source: "./assets/black_crop.png"
                    text: "Crop"
                }
                onClicked: {
                    loader.source = "Crop_Rectangle.qml"
                    cropContainer.state = "expanded"

                }
            }

            Rectangle {
                id: cropContainer
                width: 170
                height: 60
                color: "slateblue"
                visible: false
                anchors.top: cropButton.bottom
                anchors.horizontalCenter: cropButton.horizontalCenter

                GridLayout {
                    columns: 2
                    //                    columnSpacing: 5
                    //                    rowSpacing: 5
                    //                    margins: 5
                    anchors.fill: parent

                    Undo_Button {

                        id: cropundoButton; Layout.preferredWidth: 70; Layout.preferredHeight: 30; control_color: item.control_color; Layout.leftMargin: 5;
                        onButtonClicked: {
                            //                            cropRectangle.visible = false;
                            loader.source = ""
                            cropContainer.state = "closed"
                        }

                    }

                    Save_Button {id: cropsaveButton; Layout.preferredWidth: 70; Layout.preferredHeight: 30; control_color: item.control_color;
                        onButtonClicked: {
                            var dict = {
                                "y1" : loader.item.cropRectY/image.height,
                                "y2" : (loader.item.cropRectY + loader.item.cropRectHeight)/image.height,
                                "x1" : loader.item.cropRectX/image.width,
                                "x2" : (loader.item.cropRectX + loader.item.cropRectWidth)/image.width,
                                "source" : source
                            }

                            cropImage (dict)
                            backButtonClicked()

                        }
                    }
                }

                state: "closed"

                states: [
                    State {
                        name: "closed"
                        PropertyChanges {target: cropContainer; visible: false}
                    },
                    State {
                        name: "expanded"
                        PropertyChanges {target: cropContainer; visible: true}
                    }
                ]
            }
        }

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.preferredWidth: 90
            Layout.preferredHeight: 40

            Button {
                id: brightnessButton
                //        text: "G-Drive"
                //            Layout.fillWidth: true
                //            Layout.fillHeight: true
                //            Layout.preferredWidth: 90
                //            Layout.preferredHeight: 40
                anchors.fill: parent
                hoverEnabled: false

                background: Button_Background {
                    //                id: button_background
                    width: 90
                    height: 40
                    //                anchors.fill: gdriveButton
                    color:item.control_color == "black"? "gray": "lightgray"
                    source: "./assets/black_brightness.png"
                    text: "Brightness"
                }
                onClicked: {
                    image_brightness.enabled = true
                    brightnessContainer.state = "expanded"
                }

            }

            Rectangle {
                id: brightnessContainer
                width: 200
                height: 150
                color: "slateblue"
                visible: false
                anchors.top: brightnessButton.bottom
                anchors.horizontalCenter: brightnessButton.horizontalCenter

                GridLayout {
                    columns: 2
                    //                    columnSpacing: 5
                    //                    rowSpacing: 5
                    //                    margins: 5
                    anchors.fill: parent

                    Slider{id: brightnessSlider; Layout.columnSpan: 2; from: -1; to: 1; value: 0.0}

                    Undo_Button {id: brightnessundoButton; Layout.preferredWidth: 70; Layout.preferredHeight: 30; control_color: item.control_color; Layout.leftMargin: 5;
                        onButtonClicked: {
                            image_brightness.enabled = false
                            brightnessSlider.value = 0
                            brightnessContainer.state = "closed"
                        }
                    }

                    Save_Button {id: brightnesssaveButton; Layout.preferredWidth: 70; Layout.preferredHeight: 30; control_color: item.control_color;
                        onButtonClicked: {
                            var dict = {
                                "brightness" : brightnessSlider.value,
                                "source" : source
                            }

                            brightImage(dict)
                            backButtonClicked()
                        }
                    }
                }

                state: "closed"

                states: [
                    State {
                        name: "closed"
                        PropertyChanges {target: brightnessContainer; visible: false}
                    },
                    State {
                        name: "expanded"
                        PropertyChanges {target: brightnessContainer; visible: true}
                    }
                ]
            }
        }


        Item {
            //            Layout.fillWidth: true
            //            Layout.fillHeight: true
            Layout.preferredWidth: 90
            Layout.preferredHeight: 40

            Button {
                id: sharpnessButton

                anchors.fill: parent
                hoverEnabled: false

                background: Button_Background {
                    //                id: button_background
                    width: 90
                    height: 40
                    //                anchors.fill: gdriveButton
                    color:item.control_color == "black"? "gray": "lightgray"
                    source: "./assets/black_sharpness.png"
                    text: "Sharpness"
                }
                onClicked: {
                    sharpImage(source);
                    backButtonClicked()
                }
            }

            //            Rectangle {
            //                id: sharpnessContainer
            //                width: 200
            //                height: 150
            //                color: "slateblue"
            //                visible: false
            //                anchors.top: sharpnessButton.bottom
            //                anchors.horizontalCenter: sharpnessButton.horizontalCenter

            //                GridLayout {
            //                    columns: 2
            //                    //                    columnSpacing: 5
            //                    //                    rowSpacing: 5
            //                    //                    margins: 5
            //                    anchors.fill: parent

            //                    Slider{id: sharpnessSlider ; Layout.columnSpan: 2}

            //                    Undo_Button {id: sharpnessundoButton; Layout.preferredWidth: 70; Layout.preferredHeight: 30; control_color: item.control_color; Layout.leftMargin: 5; onButtonClicked: sharpnessContainer.state = "closed"}

            //                    Save_Button {id: sharpnesssaveButton; Layout.preferredWidth: 70; Layout.preferredHeight: 30; control_color: item.control_color}
            //                }

            //                state: "closed"

            //                states: [
            //                    State {
            //                        name: "closed"
            //                        PropertyChanges {target: sharpnessContainer; visible: false}
            //                    },
            //                    State {
            //                        name: "expanded"
            //                        PropertyChanges {target: sharpnessContainer; visible: true}
            //                    }
            //                ]
            //            }
        }




    }

    BrightnessContrast {
        id: image_brightness
        anchors.fill: image
        z: -1
        source: image
        brightness: brightnessSlider.value
        enabled: false
    }

    Image {
        id: image
        source: item.source
        z: -2
        cache: false

        anchors.left: parent.left
        anchors.right:parent.right
        anchors.bottom: parent.bottom
        anchors.top: topRow.bottom
        anchors.leftMargin: 50
        anchors.rightMargin: 50
        anchors.topMargin: 50
        anchors.bottomMargin:100

        property bool cropflag: true
        property bool pointflag: true

        Loader {
            id: loader

            property var obj

            onLoaded: {
                obj = {
                    "width" : image.width,
                    "height" : image.height
                }
                binder.target = loader.item
            }
        }

        Binding {
            id: binder

            property: "dict"
            value: loader.obj
        }


    }

}
