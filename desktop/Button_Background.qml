import QtQuick 2.15

Rectangle {
    property alias source: image.source
    property alias text: textfield.text

    radius: 3

    Image {
        id:image
        fillMode: Image.PreserveAspectFit
        width: parent.width * 0.2
        height: parent.height
        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter
        anchors.leftMargin: 5
        //        source: "file"
    }

    Text {
        id:textfield
        width: parent.width * 0.8
        //        height: parent.height
        anchors.left: image.right
        anchors.verticalCenter: parent.verticalCenter
        anchors.leftMargin: 5
        font.pointSize: 8
        //        anchors.topMargin: 15
    }
}
