import QtQuick 2.12
import QtQuick.Controls 2.12

Rectangle{

    property alias model: drawer_list.model
    ListView {
        id: drawer_list
        focus: true
        clip: true
        anchors.fill: parent
        //        model: navigation_model
        spacing: 5

        delegate: ItemDelegate {
            id:delegate
            width: drawer_list.width
            //                height: contentItem.height + 5
            text: model.label
            //                anchors.leftMargin: 10
            anchors.left: parent.left
            //                anchors.verticalCenter: parent.verticalCenter
            //                highlighted: ListView.isCurrentItem
            MouseArea {
                id: mousearea
                anchors.fill: parent
                //                width: delegate.width
                //                height: delegate.height
                hoverEnabled: true

                onEntered: delegate.highlighted = true

                onExited: delegate.highlighted = false

                onClicked:{
                    //                    console.log("hello")
                    //                    drawer.close()
                    model.triggered()
                    console.log(delegate.text)
                }

            }

        }
    }
}
