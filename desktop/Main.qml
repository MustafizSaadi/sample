import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.12

ApplicationWindow {
    id: item
    width: 850
    height: 700
    color: item.color_theme()
    visible: true

    property var control_color: item.control_color_theme()

    //    property var theme: item.color

    property var source
    property var image_index
    property int image_count
    //    property int album_index: 0
    property var upload_dict
    property var album_dict
    property var image_details_dict
    property var update_dict


    signal newAlbum(new_album_dict: var)
    signal imageUploaded(dict: var)
    signal getData()
    signal dataUpdated(dict: var)
    signal albumUpdated(dict: var)
    signal insertImages(dict: var)
    signal deleteImages(source: var)
    signal viewAlbumChanged(id: int)
    signal download(filename: var)
    signal pageClicked(data: var)
    signal resizeImage(dict: var)
    signal cropImage(dict: var)
    signal brightImage(dict: var)
    signal sharpImage(dict: var)
    signal image_edited()
    signal nextClicked()
    signal backClicked()


    function color_theme() { //run a loop in themes_list and take background and control_color from their
        if(_user.theme == "Light"){
            console.log(_user.theme)
            return "white"
        }
        else if(_user.theme == "Dark"){
            return "black"
        }
    }

    function control_color_theme() {
        if(_user.theme == "Light"){
            console.log(_user.theme)
            return "black"
        }
        else if(_user.theme == "Dark"){
            return "white"
        }
    }


    function sendToCloud(eventType, eventDescription) {
        var xmlhttp = new XMLHttpRequest();
        var url = "https://api.amplitude.com/2/httpapi";

        xmlhttp.open('POST', url, true);

        var event = {
            "description" :eventDescription
        }

        var mp = {
            "event_properties" : event,
            "event_type" : eventType,
            "device_id" : _deviceId
        }
//        var json = JSON.stringify(mp)

        var payload = {
         "api_key": "acb4b0f746f70584fb864c9b2cc7af09",
         "events": mp
        }

//        var payload = 'api_key=acb4b0f746f70584fb864c9b2cc7af09' +
//                      '&' +
//                      'events=' + json

        console.log(payload)


        xmlhttp.setRequestHeader('Content-type', 'application/json')

        xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
            console.log("within http")
            console.log(xmlhttp.responseText);
//            if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
//                console.log("hello")
//                console.log(xmlhttp.responseText);
//            }
        }
        xmlhttp.send(JSON.stringify(payload));
    }

    ListModel {
        id: navigation_model

        ListElement {source: "./assets/white_home.png"; label: "Home"; triggered: function() {
            if(stackview.currentItem.objectName != "Home"){
                viewAlbumChanged(-1);
                sendToCloud("Home", "Page click")
                stackview.push(home)
            }
        }
        }

        ListElement {source: "./assets/white_album.png"; label: "Album"; triggered: function() {
            if(stackview.currentItem.objectName != "Album"){
                stackview.push(album)

            sendToCloud("Album", "Page click")
            console.log("album called")
            }
        }
        }

        ListElement {source: "./assets/white_palette.png"; label: "User Preference"; triggered: function() {
            if(stackview.currentItem.objectName != "User Preference"){
                stackview.push(user_preference)

            sendToCloud("User Preference", "Page click")
            }
        }
        }

        ListElement {source: "./assets/white_exit.png"; label: "Exit"; triggered: function() {
            Qt.quit()
        }
        }
    }

    SideBar {
        id: sidebar
        y: menuButton.height
        height: item.height
        color: "slateblue"
        width: 50
        model: navigation_model
        //        border.color: item.control_color

        state: "close"

        states: [
            State {
                name: "close"
                PropertyChanges {
                    target: sidebar; width: 50

                }
                PropertyChanges {
                    target: menuButton; rotation: 0
                }
            },

            State {
                name: "expanded"
                PropertyChanges {
                    target: sidebar; width: 200
                }
                PropertyChanges {
                    target: menuButton; rotation: 90
                }
            }

        ]

        Behavior on width {
            NumberAnimation {duration: 500}
        }
    }

    Loader {
        id: dialog_loader
        property var obj

        onLoaded: {

            dialog_loader.item.open()
            if(dialog_loader.state == "image_info_loader") {
                obj = {
                    "control_color": item.control_color,
                    "image_details_dict": item.image_details_dict,
                    "album_index": _album_model.get_album_index(item.image_details_dict["album_id"]),
                    "combo_model": _album_names

                }
                binder.target = dialog_loader.item
            }
            else if(dialog_loader.state == "album_input_loader") {
                obj = {
                    "control_color": item.control_color
                }
                binder.target = dialog_loader.item
            }
            else if(dialog_loader.state == "album_info_loader") {
                obj = {
                    "control_color": item.control_color,
                    "album_dict": item.album_dict,
                    "image_count": item.image_count
                }
                binder.target = dialog_loader.item
            }
        }

        state: ""

        states: [
            State {
                name: "drive_loader"
                PropertyChanges { target: dialog_loader; source: "Dialog_Drive_Info.qml";}
            },
            State {
                name: "image_info_loader"
                PropertyChanges { target: dialog_loader; source: "Dialog_Image_Details.qml"}
            },
            State {
                name: "album_info_loader"
                PropertyChanges { target: dialog_loader; source: "Dialog_Album_Details.qml" }
            },
            State {
                name: "album_input_loader"
                PropertyChanges { target: dialog_loader; source: "Dialog_Album_Input.qml" }
            }
        ]

        Binding {
            id: binder

            property: "obj"
            value: dialog_loader.obj
        }

        Connections {
            target: dialog_loader.item

            ignoreUnknownSignals: true

            function onDownload(filename) {
                download(filename)
            }

            function onClosed() {
                dialog_loader.state = ""
            }

            function onDataUpdated(dict) {
                dataUpdated(dict)
                item.image_details_dict = dict
                if("index" in dict)
                    item.image_details_dict["album_id"] = _album_model.get_album_id(dict["index"])

            }

            function onNewAlbum(dict) {
                newAlbum(dict)
            }

            function onAlbumUpdated(dict) {
                item.album_dict = dict
                albumUpdated(dict)
            }
        }
    }

    Image {
        id: menuButton
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.leftMargin: 5
        //        anchors.verticalCenter: parent.verticalCenter
        source: item.color == "#000000" ? "./assets/white_humburger.png" : "./assets/black_humburger.png"
        MouseArea {
            anchors.fill: parent
            onPressed: {
                if(sidebar.state == "close"){
                    //                    RotationAnimation{
                    sidebar.state = "expanded"
                    //                    menuButton.source = item.color == "#000000" ? "./assets/white_close.png" : "./assets/black_close.png"
                }
                else{
                    sidebar.state = "close"
                    //                    menuButton.source = item.color == "#000000" ? "./assets/white_humburger.png" : "./assets/black_humburger.png"
                }
            }
        }
        Behavior on rotation {
            RotationAnimation{duration: 500}
        }
    }

    StackView {
        id: stackview
        anchors.top: menuButton.bottom
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        initialItem: home
        onCurrentItemChanged: {
            if(currentItem.objectName == "Home"){
                console.log("home")
                viewAlbumChanged(-1);
            }
            else if(currentItem.objectName == "Album_Images"){
//                viewAlbumChanged(item.album_dict["id"])
                item.image_count = _sortimages.rowCount() ;
            }
            else if(currentItem.objectName == "edit")
                console.log("top perfect")
        }
    }

//    onImage_edited: {
//        stackview.pop()
//        stackview.pop()
//        stackview.push(home)
//    }

    Component {
        id: home

        Item {
            objectName: "Home"
            Home {
                x: sidebar.width
                id: home_view
                width: item.width - (sidebar.width)
                height: item.height - 50
                grid_model: _pageimages
                theme: {
                    console.log(item.color)
                    item.color
                }
                control_color: {
                    console.log(item.control_color)
                    item.control_color
                }

                onDownloadClicked: dialog_loader.state = "drive_loader"
                onNextButtonClicked: nextClicked()
                onBackButtonClicked: backClicked()
            }

            Connections {
                target: home_view

                ignoreUnknownSignals: true


                function onImagePressed(index) {
                    //                        console.log("Image pressed", index)
                    //                    item.image_details_dict = dict
                    //                    item.source = dict["source"]
                    //                        item.album_index = 0
                    item.image_index = index
                    stackview.push(full_image)
                }

                function onImageUploaded(path) {
                    var upload_dict = {
                        "location": "",
                        "album_id": 1,
                        "source": path,
                        "description": ""
                    }
                    console.log(upload_dict)
                    item.imageUploaded(upload_dict)

                    console.log("Image uploaded")
                }
            }
        }


    }


    Component {
        id: album

        Item {
            objectName: "Album"
            Album_List {
                x: sidebar.width
                id: album_list_view
                width: item.width - (sidebar.width)
                height: item.height - 50
                model: _sortalbums
                theme: item.color
                control_color: item.control_color

                onBackButtonClicked: stackview.pop()

                onCreateAlbumClicked: dialog_loader.state = "album_input_loader"

            }

            Connections {
                target: album_list_view

                ignoreUnknownSignals: true


                function onDelegateClicked(dict, index) {
                    console.log(index, " clicked")
                    dict["id"] = _sortalbums.getSourceId(index)
                    viewAlbumChanged(dict["id"])
                    console.log(dict["name"], dict["description"])
                    //                    item.image_count = _sortimages.rowCount()
                    item.album_dict = dict
                    //                    item.album_dict["count"] = _sortimages.rowCount() + " photos "
                    //                        item.album_index = index
                    stackview.push(album_images)
                }
            }
        }
    }


    Component {
        id: album_images

        Item {
            objectName: "Album_Images"
            Album_Images {
                x: sidebar.width
                id: album_images_view
                width: item.width - (sidebar.width)
                height: item.height - 50
                //                dict: item.album_dict
                grid_model: _sortimages
                image_count: item.image_count

                theme: item.color
                control_color: item.control_color

                onDetailsButtonClicked: {
//                    item.image_count = _sortimages.rowCount()
                    dialog_loader.state = "album_info_loader"
                }

                onBackButtonClicked: stackview.pop()
            }

            Connections {
                target: album_images_view

                ignoreUnknownSignals: true


                function onImagePressed(ind) {
                    item.image_index = ind
                    //                        console.log("Image pressed", index)
                    //                    item.source = dict["source"]
                    //                    /* I need make sure that this index is album wise */
                    //                    item.image_details_dict = dict
                    stackview.push(full_image)
                }
            }
        }
    }

    Component {
        id: full_image

        Item {

            Full_Image {
                id: full_image_view
                x: sidebar.width
                width: {console.log(item.width - (sidebar.width)); item.width - (sidebar.width)}
                height: item.height
                model:_sortimages
                index: item.image_index
                //                source: item.source
                theme: item.color
                control_color: item.control_color

                onBackButtonClicked: stackview.pop()
            }

            Connections {
                target: full_image_view

                ignoreUnknownSignals: true

                function onDetailsButtonClicked(dict) {
                    item.image_details_dict = dict
                    console.log("dict", dict["album_id"])
                    //                    image_details_pop_up.open()
                    dialog_loader.state = "image_info_loader"
                }

                function onEditButtonClicked(source) {
                    item.source = source
                    console.log("stackview pushed")
                    stackview.push(edit_image)
                }
            }
        }
    }

    Component {
        id: edit_image

        Item {
            objectName: "edit"
            Edit_Image {
                id: edit_image_view
                x: sidebar.width
                width: item.width - (sidebar.width)
                height: item.height - 50
                theme: item.color
                control_color: item.control_color

                source: {console.log(item.source); item.source}
                onBackButtonClicked: stackview.pop()

            }

            Connections {
                target: edit_image_view

                function onResizeImage(dict){
                    resizeImage(dict)
//                    stackview.pop();
//                    stackview.pop();

                }

                function onCropImage(dict){
                    cropImage(dict)
//                    stackview.pop();
//                    stackview.pop();
                }

                function onBrightImage(dict){
                    brightImage(dict)
//                    stackview.pop();
//                    stackview.pop();
//                    stackview.push(home)
                }

                function onSharpImage(dict){
                    sharpImage(dict)
//                    stackview.pop();
//                    stackview.pop();
                }
            }
        }
    }


    Component {
        id: user_preference

        Item {
            objectName: "User Preference"
            User_Preference {
                x: sidebar.width
                id: user_preference_view
                width: item.width - (sidebar.width)
                height: item.height - 50
                button_model: _themes_model
                image_model: _themes_model
                color: item.color
                theme: _user.theme

                //                    dark_flag: item.color == "#000000" ? true : false
                control_color: item.control_color

                onBackButtonClicked: stackview.pop()

                onThemeChanged: {
                    _user.theme = user_preference_view.theme
                    console.log(user_preference_view.theme, " from main qml")
                }
            }

        }

    }


}
//}
