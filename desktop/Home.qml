import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import Qt.labs.platform 1.1
//import QtQuick.Dialogs 1.2


Item {
    id:item

    property var grid_model
    property var theme
    property var control_color

    signal nextButtonClicked()
    signal backButtonClicked()
    signal imagePressed(index: int)
    signal imageUploaded(path: var)
    //    signal uploadClicked()
    signal downloadClicked()

    Image {
        id: nextButton
        //        text: "Next"
        width: 30
        height: 30
        fillMode: Image.PreserveAspectFit
        source: item.theme == "#000000" ? "./assets/white_next_arrow.png" : "./assets/black_next_arrow.png"

        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.top: parent.top
        anchors.topMargin: 10
        visible: grid_model.next_flag
        MouseArea {
            anchors.fill: parent
            onClicked: nextButtonClicked()
        }
    }

    Image {
        id: backButton
        //        text: "Back"
        width: 30
        height: 30
        fillMode: Image.PreserveAspectFit

        source: item.theme == "#000000" ? "./assets/white_back_arrow.png" : "./assets/black_back_arrow.png"
        anchors.right: nextButton.left
        anchors.rightMargin: 10
        anchors.top: parent.top
        anchors.topMargin: 10
        visible: grid_model.back_flag
        MouseArea {
            anchors.fill: parent
            onClicked: backButtonClicked()
        }
    }

    RowLayout {

        anchors.top: parent.top
        anchors.left: parent.left
        anchors.leftMargin: 10

        spacing: 10

        Button {
            id: gdriveButton
            //        text: "G-Drive"
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.preferredWidth: 90
            Layout.preferredHeight: 40
            hoverEnabled: false

            background: Button_Background {
                id: button_background
                width: 90
                height: 40
                //                anchors.fill: gdriveButton
                color:item.control_color == "black"? "gray": "lightgray"
                source: "./assets/google_drive.png"
                text: "G-Drive"
            }
            onClicked: { console.log("drive")
                downloadClicked()}

        }

        Button {
            id: uploadButton
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.preferredWidth: 90
            Layout.preferredHeight: 40
            hoverEnabled: false
            background: Button_Background {
                //                anchors.fill: uploadButton
                width: 90
                height: 40
                color:item.control_color == "black"? "gray": "lightgray"
                source: "./assets/black_upload.png"
                text: "Upload"
            }
            onClicked: fileDialog.open()
        }

        Button {
            id: pictureButton
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.preferredWidth: 90
            Layout.preferredHeight: 40
            hoverEnabled: false
            property var window
            background: Button_Background {
                width: 90
                height: 40
                //                anchors.fill: pictureButton
                color:item.control_color == "black"? "gray": "lightgray"
                source: "./assets/black_camera.png"
                text: "Camera"
            }
            onClicked: {
                var component = Qt.createComponent("Camera_View.qml")
                pictureButton.window   = component.createObject()
                pictureButton.window.show()

            }

            Connections {
                target: pictureButton.window

                ignoreUnknownSignals: true


                function onNewImage(path) {
                    console.log("Image pressed")
                    imageUploaded("file:///"+path)
                }
            }
        }
    }


    Image_Grid {
        id: image_grid
        width: parent.width
        height: parent.height - 50
        model: grid_model
        theme: item.theme
        control_color: item.control_color
        anchors.top: backButton.bottom
        anchors.topMargin: 20
        //        onImagePressed: imagePressed(source)
    }


    FileDialog {
        id: fileDialog
        title: "Please choose a file"
        folder: ""
        nameFilters: ["Image files (*.png *.jpeg *jpg)"]
        onAccepted: {
            imageUploaded(fileDialog.file)
        }
        onRejected: {
            console.log("Canceled")
        }
    }


    Connections {
        target: image_grid

        ignoreUnknownSignals: true


        function onImagePressed(ind) {
            console.log("Image pressed")
            item.imagePressed(ind)
        }
    }
}
