import QtQuick 2.0
import QtQuick.Controls 2.12

Dialog {
    id: album_details_pop_up
    width: 400
    height: 400
    parent: Overlay.overlay
    anchors.centerIn: parent
    title: "Album Details"
    //        visible: false
    closePolicy: Dialog.NoAutoClose

    property var obj

    signal closed()
    signal albumUpdated(dict: var)

    modal: true

    background: Rectangle {
        width: album_details_pop_up.width
        height: album_details_pop_up.height
        color: "slateblue"
        clip: true
    }


    contentItem: Album_details {
        id: album_details_view
        width: 400
        height: 400
        //            clip: true
        //            //            anchors.fill: parent
        //            color: item.color
        control_color: obj["control_color"]
        prev_dict: obj["album_dict"]
        //            theme: item.color
        count: obj["image_count"]
        onBackButtonClicked: {
            closed()
            album_details_pop_up.close()
        }
    }

    Connections {
        target: album_details_view

        ignoreUnknownSignals: true

        function onDataUpdated(dict) {
            albumUpdated(dict)
        }

    }
}
