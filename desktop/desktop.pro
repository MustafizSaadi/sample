QT += quick
QT += multimedia
QT += widgets
QT += gui

CONFIG += c++17
#CONFIG+= debug

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../core/release/ -lcore
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../core/debug/ -lcore
else:unix: LIBS += -L$$OUT_PWD/../core/ -lcore
#else:android: LIBS += -L$$OUT_PWD/../core/ -lcore

INCLUDEPATH += $$PWD/../core
DEPENDPATH += $$PWD/../core

DISTFILES += \
    Drive_Info.qml

#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../3rdParty/opencv/lib/ -lopencv_world453
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../3rdParty/opencv/lib/ -lopencv_world453d
#else:unix: LIBS += -L$$PWD/../3rdParty/opencv/lib/ -lopencv_world453

#INCLUDEPATH += $$PWD/../3rdParty/opencv/include
#DEPENDPATH += $$PWD/../3rdParty/opencv/include
