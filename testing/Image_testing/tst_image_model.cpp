#include <QtTest>
#include <QAbstractItemModelTester>
#include <QCoreApplication>
// add necessary includes here
#include "image.h"
#include "album.h"

class image_model : public QObject
{
    Q_OBJECT

public:
    image_model();
    ~image_model();

private slots:
    void test_model_implementation();
    void test_insert();
    void test_update();

private:
    Image *image;
    Album *album;
};

image_model::image_model()
{
    album = new Album();
    image =new Image(nullptr, album);
}

image_model::~image_model()
{
    delete album;
    delete image;
}

void image_model::test_model_implementation()
{
    new QAbstractItemModelTester(image);
}

void image_model::test_insert()
{
    QMap<QString, QVariant> data;
    data["location"] = QVariant("");
    data["description"] = QVariant("");
    data["source"] = QVariant("./data/robot_croped.jpg");
    data["album_id"] = 1;

    image->insert_data(QVariant(data));

    QCOMPARE(image->rowCount(), 1);
}

void image_model::test_update()
{
    QMap<QString, QVariant> data;
    data["location"] = QVariant("");
    data["description"] = QVariant("Hi! I am robot");
    data["source"] = QVariant("./data/robot_croped.jpg");
    data["album_id"] = 1;

    image->update_data(QVariant(data));

    QCOMPARE(image->index(0).data(Image::descriptionRole), "Hi! I am robot");
}

QTEST_APPLESS_MAIN(image_model)

#include "tst_image_model.moc"
