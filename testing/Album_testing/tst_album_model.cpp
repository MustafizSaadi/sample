#include <QtTest>
#include <QAbstractItemModelTester>
// add necessary includes here

#include "album.h"

class album_model : public QObject
{
    Q_OBJECT

public:
    album_model();
    ~album_model();

private slots:
    void test_model_implementation();
    void test_insert();
    void test_update();

private:
    Album *album;

};

album_model::album_model()
{
    album = new Album(nullptr);
}

album_model::~album_model()
{
    delete album;
}

void album_model::test_model_implementation()
{
    new QAbstractItemModelTester(album);
}

void album_model::test_insert()
{
    QMap<QString, QVariant> mp;
    mp["name"] = QVariant("Practice");
    mp["description"] = QVariant("For testing");

    album->insert_album(QVariant(mp));

    QCOMPARE(album->rowCount(), 2);
}

void album_model::test_update()
{
    QMap<QString, QVariant> mp;

    mp["name"] = QVariant("Practice");
    mp["description"] = QVariant("testing");
    mp["id"] = 1;

    album->update_album(QVariant(mp));

    QCOMPARE(album->index(0).data(Album::descriptionRole), "testing");
}


QTEST_APPLESS_MAIN(album_model)

#include "tst_album_model.moc"
