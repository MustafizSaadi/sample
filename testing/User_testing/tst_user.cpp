#include <QtTest>
#include <QDebug>
// add necessary includes here
#include "user.h"

class user : public QObject
{
    Q_OBJECT

public:
    user();
    ~user();

private slots:
    void test_theme();

private:
    User *usr;

};

user::user()
{
    usr = new User();
}

user::~user()
{
    delete usr;
}

void user::test_theme()
{
    QSignalSpy spy(usr, &User::themeChanged);

    usr->setTheme("Dark");

    QCOMPARE(spy.count(), 1);
    QCOMPARE(usr->getTheme(), "Dark");
}

QTEST_APPLESS_MAIN(user)

#include "tst_user.moc"
