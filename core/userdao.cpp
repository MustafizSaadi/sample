#include "userdao.h"
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>

UserDao::UserDao(QSqlDatabase& database): m_database(database)
{}

bool UserDao::init() const
{
    bool success = true;

    const QString createSQL = "CREATE TABLE IF NOT EXISTS user ("
                               "                ID INTEGER PRIMARY KEY AUTOINCREMENT,"
                               "                preferred_theme VARCHAR"
                               ");";

    QSqlQuery query(m_database);

    if (query.exec(createSQL)) {
        qDebug() << "user Table creation query execute successfully";
    } else {

        const QSqlError error = query.lastError();
        qDebug() << error.text();
        success = false;
    }

    return success;
}

bool UserDao::insert_default_theme(QString theme) const
{
    bool success = true;

    const QString createSQL = "INSERT INTO user (preferred_theme)"
            " values (:theme)";

    QSqlQuery query(m_database);

    query.prepare(createSQL);

    query.bindValue(":theme", theme);


    if (query.exec()) {
//        qDebug() << "Data inserted successfully";
        this->sendToCloud("Database hit", "Data inserted successfully to User table");
    } else {
        const QSqlError error = query.lastError();
//        qDebug() << error.text();
        success = false;
        this->sendToCloud("Database miss", error.text());
    }

   return success;

}

QString UserDao::read_preferred_theme() const
{
    const QString createSQL = "SELECT preferred_theme FROM user";

    QSqlQuery query(m_database);

    if (query.exec(createSQL)) {
//        qDebug() << "Data selected successfully";
        this->sendToCloud("Database hit", "Data selected successfully from user table");
    } else {

        const QSqlError error = query.lastError();
//        qDebug() << error.text();
        this->sendToCloud("Database miss", error.text());
    }

    QString theme;

    while (query.next()) {
        theme = query.value(0).toString();
    }
    qDebug() << theme;
    return theme;
}

bool UserDao::update_preferred_theme(QString theme) const
{
    bool success = true;

    const QString createSQL = "UPDATE user "
        "SET preferred_theme = :newtheme "
        "WHERE ID = 1";

    QSqlQuery query(m_database);
    qDebug() << createSQL ;

    query.prepare(createSQL);

    query.bindValue(":newtheme", theme);

    if (query.exec()) {
//        qDebug() << "Data updated successfully";
        this->sendToCloud("Database hit", "Data updated successfully to user table");
    } else {
        const QSqlError error = query.lastError();
//        qDebug() << error.text();
        success = false;
        this->sendToCloud("Database miss", error.text());
    }

    return success;
}

void UserDao::sendToCloud(QString eventType, QString eventDescription) const
{
    QMap <QString, QVariant> mp;
    QMap <QString, QVariant> event;

    event["description"] = QVariant(eventDescription);

    mp["event_properties"] = QVariant(event);
    mp["event_type"] = QVariant(eventType);
    mp["device_id"] = QVariant(QSysInfo::machineUniqueId());
    QJsonObject json = QVariant(mp).toJsonObject();

    QJsonObject payload;
//        qDebug() << "Error in creating tables";

    QString createUrl = "https://api.amplitude.com/2/httpapi";

//        QUrl url(createUrl);
//        QUrlQuery query;
    payload["api_key"] = "acb4b0f746f70584fb864c9b2cc7af09";
    payload["events"] =  json;

    QJsonDocument doc(payload);


    QNetworkAccessManager *mgr = new QNetworkAccessManager();
    QUrl url(createUrl);

    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    QNetworkReply *reply = mgr->post(request, doc.toJson());

    QObject::connect(reply, &QNetworkReply::finished, [=] () {
        if(reply->error() == QNetworkReply::NoError){
            qDebug() << "Data sent to cloud" ;
        }
        else {

            qDebug() << reply->readAll();
        }
    });
}
