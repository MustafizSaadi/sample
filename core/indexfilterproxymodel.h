#ifndef INDEXFILTERPROXYMODEL_H
#define INDEXFILTERPROXYMODEL_H

#include <QSortFilterProxyModel>

#include "core_global.h"

class CORE_EXPORT IndexFilterProxyModel: public QSortFilterProxyModel
{
    Q_OBJECT

    Q_PROPERTY(int threshold_index READ threshold_index WRITE setThreshold_index NOTIFY threshold_indexChanged)
    Q_PROPERTY(bool back_flag READ back_flag WRITE setBack_flag NOTIFY back_flagChanged)
    Q_PROPERTY(bool next_flag READ next_flag WRITE setNext_flag NOTIFY next_flagChanged)


public:
    explicit IndexFilterProxyModel(QObject *parent = nullptr);

    int threshold_index() const;
    bool back_flag() const;
    bool next_flag() const;

public slots:
    void setThreshold_index(int index);
    void setBack_flag(bool flag);
    void setNext_flag(bool flag);
    void validate_index(int index);

    void inc_index();
    void dec_index();

signals:
    void threshold_indexChanged();
    void back_flagChanged();
    void next_flagChanged();

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;

private:
    int m_threshold_index;
    bool m_back_flag;
    bool m_next_flag;
};

#endif // INDEXFILTERPROXYMODEL_H
