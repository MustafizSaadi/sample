#ifndef ALBUMDAO_H
#define ALBUMDAO_H

#include "album.h"
#include "core_global.h"

#include <QtSql/QSqlDatabase>

class CORE_EXPORT AlbumDao
{
public:
    AlbumDao(QSqlDatabase& database);
    bool init() const;
    QVector <Album::Entry_album*> read_all_album() const;
    bool insert_album(Album::Entry_album* album) const;
    bool delete_album(int id) const;
    bool update_album(Album::Entry_album* album) const;

private:
    QSqlDatabase& m_database;
    void sendToCloud(QString eventType, QString eventDescription) const;


};

#endif // ALBUMDAO_H
