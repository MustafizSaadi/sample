#ifndef ALBUMDATETIMEPROXYMODEL_H
#define ALBUMDATETIMEPROXYMODEL_H

#include "core_global.h"

#include <QSortFilterProxyModel>

class CORE_EXPORT AlbumDatetimeProxyModel: public QSortFilterProxyModel
{
    Q_OBJECT
public:
    explicit AlbumDatetimeProxyModel(QObject *parent = nullptr);

    Q_INVOKABLE int getSourceId(int index);
protected:
    bool lessThan(const QModelIndex &sourceLeft, const QModelIndex &sourceRight) const override ;
};

#endif // ALBUMDATETIMEPROXYMODEL_H
