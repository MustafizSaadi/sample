#ifndef IMAGEDATETIMESORTPROXYMODEL_H
#define IMAGEDATETIMESORTPROXYMODEL_H

#include "core_global.h"

#include <QSortFilterProxyModel>

class CORE_EXPORT ImageDatetimeSortProxyModel: public QSortFilterProxyModel
{
    Q_OBJECT
public:
    explicit ImageDatetimeSortProxyModel(QObject *parent = nullptr);

protected:
    bool lessThan(const QModelIndex &sourceLeft, const QModelIndex &sourceRight) const override ;
};

#endif // IMAGEDATETIMESORTPROXYMODEL_H
