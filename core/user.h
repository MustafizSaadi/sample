#ifndef USER_H
#define USER_H

#include "core_global.h"
#include <QObject>
//#include "databasemanager.h"
//#include "userdao.h"
class DatabaseManager;

class CORE_EXPORT User : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString theme READ getTheme WRITE setTheme NOTIFY themeChanged)
public:
    explicit User(QObject *parent = nullptr);

    QString getTheme();

    QString theme;

public slots:
    void setTheme(QString theme);

signals:
    void themeChanged();

private:
    DatabaseManager &dbm;
//    QString theme;

};

#endif // USER_H
