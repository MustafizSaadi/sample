QT += gui
QT += qml
QT += sql
QT += networkauth quickcontrols2
#QT += core5compat
QT += core
QT += multimedia

TEMPLATE = lib
DEFINES += CORE_LIBRARY

CONFIG += c++17
#CONFIG+= debug

#INCLUDEPATH += "C:/Qt/6.1.3/mingw81_64/include/QtNetworkAuth"

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    album.cpp \
    albumdao.cpp \
    albumdatetimeproxymodel.cpp \
    databasemanager.cpp \
    downloadthread.cpp \
    filterbyalbumproxymodel.cpp \
    googleauth.cpp \
    image.cpp \
    imagedao.cpp \
    imagedatetimesortproxymodel.cpp \
    indexfilterproxymodel.cpp \
    themes.cpp \
    user.cpp \
    userdao.cpp

HEADERS += \
    album.h \
    albumdao.h \
    albumdatetimeproxymodel.h \
    core_global.h \
    databasemanager.h \
    downloadthread.h \
    filterbyalbumproxymodel.h \
    googleauth.h \
    image.h \
    imagedao.h \
    imagedatetimesortproxymodel.h \
    indexfilterproxymodel.h \
    themes.h \
    user.h \
    userdao.h

# Default rules for deployment.
unix {
    target.path = /usr/lib
}
#android {
#    target.path = /usr/lib
#}
!isEmpty(target.path): INSTALLS += target

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../3rdParty/opencv/lib/ -lopencv_world453
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../3rdParty/opencv/lib/ -lopencv_world453d
else:unix: LIBS += -L$$PWD/../3rdParty/opencv/lib/ -lopencv_features2d -lopencv_highgui -lopencv_imgcodecs -lopencv_imgproc

INCLUDEPATH += $$PWD/../3rdParty/opencv/include
DEPENDPATH += $$PWD/../3rdParty/opencv/include

DISTFILES += \
    assets/dummy-image-square.jpg

#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../3rdParty/opencv/lib/ -lopencv_features2d453
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../3rdParty/opencv/lib/ -lopencv_features2d453d
#else:unix: LIBS += -L$$PWD/../3rdParty/opencv/lib/ -lopencv_features2d453

INCLUDEPATH += $$PWD/../3rdParty/opencv/include
DEPENDPATH += $$PWD/../3rdParty/opencv/include
