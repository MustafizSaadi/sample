#include <QDebug>

#include "indexfilterproxymodel.h"

//#include "image.h"

IndexFilterProxyModel::IndexFilterProxyModel(QObject *parent)
    : QSortFilterProxyModel(parent), m_threshold_index(0), m_back_flag(false), m_next_flag(true)
{}

int IndexFilterProxyModel::threshold_index() const
{
    return m_threshold_index;
}

bool IndexFilterProxyModel::back_flag() const
{
    return m_back_flag;
}

bool IndexFilterProxyModel::next_flag() const
{
    return m_next_flag;
}

void IndexFilterProxyModel::validate_index(int index)
{
    if(index < 0 || index >= sourceModel()->rowCount() || index == m_threshold_index)
        return;
    setThreshold_index(index);
}

void IndexFilterProxyModel::setThreshold_index(int index)
{
    if(index == 0)
        setBack_flag(false);
    else
        setBack_flag(true);

    if(index + 9 >= sourceModel()->rowCount())
        setNext_flag(false);
    else
        setNext_flag(true);

    m_threshold_index = index;

    emit threshold_indexChanged();

    invalidateFilter();

    //qDebug() << "invalidateFilter";
}

void IndexFilterProxyModel::setBack_flag(bool flag)
{
    if(m_back_flag != flag){
        m_back_flag = flag;
        emit back_flagChanged();
    }

}

void IndexFilterProxyModel::setNext_flag(bool flag)
{
    if(m_next_flag != flag){
        m_next_flag = flag;
        emit next_flagChanged();
    }

}

bool IndexFilterProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
    //const QModelIndex index = sourceModel()->index(sourceRow, 0, sourceParent);
    qDebug() << sourceRow << " " << m_threshold_index;
    if(m_threshold_index <= sourceRow && sourceRow < m_threshold_index + 9)
        return true;

    return false;
}

void IndexFilterProxyModel::inc_index()
{
    qDebug() <<m_threshold_index;
    validate_index(m_threshold_index + 9);
}

void IndexFilterProxyModel::dec_index()
{
    qDebug() <<m_threshold_index;
    validate_index(m_threshold_index - 9);
}

