#ifndef MODEL_H
#define MODEL_H

#include <QAbstractListModel>

#include "core_global.h"

class CORE_EXPORT Model:public QAbstractListModel
{
    Q_OBJECT
public:
    explicit Model(QObject* parent = nullptr);
};

#endif // MODEL_H
