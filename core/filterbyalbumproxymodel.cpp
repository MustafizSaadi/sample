#include "filterbyalbumproxymodel.h"
#include "image.h"

FilterbyAlbumProxyModel::FilterbyAlbumProxyModel(QObject *parent):
    QSortFilterProxyModel(parent), m_album_id(-1)
{}

int FilterbyAlbumProxyModel::album_id() const
{
    return m_album_id;
}

void FilterbyAlbumProxyModel::setAlbum_id(int index)
{
    if(m_album_id != index){
        m_album_id = index;

        emit album_idChanged();

        invalidateFilter();
    }
}

bool FilterbyAlbumProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
    //const QModelIndex index = sourceModel()->index(sourceRow, 0, sourceParent);
    //qDebug() << sourceRow << " " << m_threshold_index;
    if(m_album_id < 0)
        return true;
    else{
        QModelIndex index = sourceModel()->index(sourceRow, 0, sourceParent);
        if((sourceModel()-> data(index, Image::album_idRole).toInt() == m_album_id))
                return true;
         else
                return false;
    }

    return false;
}

//int FilterbyAlbumProxyModel::get_source_index(int index)
//{
//    QModelIndex sourceIndex = mapToSource(in);
//    return mapToSource(QModelIndex(index)).row();
//}
