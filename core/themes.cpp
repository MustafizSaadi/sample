#include "themes.h"

Themes::Themes(QObject *parent) : QAbstractListModel(parent)
{
    theme temp;

    temp.name = "Dark";
    temp.imageSource = "file:///C:/Users/mustafizur/Pictures/dark_example.png";

    themes.append(temp);

    temp.name = "Light";
    temp.imageSource = "file:///C:/Users/mustafizur/Pictures/light_example.png";

    themes.append(temp);

}

int Themes::rowCount(const QModelIndex &parent) const {
    return parent.isValid()? 0 : themes.count();
}

QVariant Themes::data(const QModelIndex &index, int role) const {
    if (index.row() < 0 || index.row()>= themes.count())
        return QVariant();

    switch (role) {
    case textRole:
        return themes[index.row()].name;
    case sourceRole:
        return themes[index.row()].imageSource;
    default:
        return QVariant();
    }
}

QHash<int, QByteArray> Themes::roleNames() const{
    QHash<int, QByteArray> names;

    names.insert(textRole, "text");
    names.insert(sourceRole, "source");

    return names;
}

