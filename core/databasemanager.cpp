#include "databasemanager.h"
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QJsonObject>
#include <QUrlQuery>
#include <QJsonDocument>

DatabaseManager& DatabaseManager::instance()
{
    static DatabaseManager singleton;
    return singleton;
}

DatabaseManager::DatabaseManager(const QString &path):
    mDatabase(new QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE"))),
    albumdao(*mDatabase),
    imagedao(*mDatabase),
    userdao(*mDatabase)
{
    mDatabase->setDatabaseName(path);
    mDatabase->open();

    if(albumdao.init() && imagedao.init() && userdao.init()){

        sendToCloud("Database hit", "All the tables created successfully");

//        qDebug() << "All the tables created successfully";
    }
    else {

        sendToCloud("Database Miss", "All the tables are not created successfully");


    }

//    if(imagedao.add_column())
//         qDebug() << "New column added successfully";
}

DatabaseManager::~DatabaseManager()
{
    mDatabase->close();
    delete mDatabase;
}

void DatabaseManager::sendToCloud(QString eventType, QString eventDescription)
{
    QMap <QString, QVariant> mp;
    QMap <QString, QVariant> event;

    event["description"] = QVariant(eventDescription);

    mp["event_properties"] = QVariant(event);
    mp["event_type"] = QVariant(eventType);
    mp["device_id"] = QVariant(QSysInfo::machineUniqueId());
    QJsonObject json = QVariant(mp).toJsonObject();

    QJsonObject payload;
//        qDebug() << "Error in creating tables";

    QString createUrl = "https://api.amplitude.com/2/httpapi";

//        QUrl url(createUrl);
//        QUrlQuery query;
    payload["api_key"] = "acb4b0f746f70584fb864c9b2cc7af09";
    payload["events"] =  json;

    QJsonDocument doc(payload);


    QNetworkAccessManager *mgr = new QNetworkAccessManager();
    QUrl url(createUrl);

    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    QNetworkReply *reply = mgr->post(request, doc.toJson());

    QObject::connect(reply, &QNetworkReply::finished, [=] () {
        if(reply->error() == QNetworkReply::NoError){
            qDebug() << "Data sent to cloud" ;
        }
        else {

            qDebug() << reply->readAll();
        }
    });
}
