#include "image.h"
#include "databasemanager.h"
#include <QCoreApplication>
#include <QtQml/QQmlEngine>
#include <QQmlApplicationEngine>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
using namespace cv;

Image::Image(QObject *parent, Album* album) : QAbstractListModel(parent),
    dbm(DatabaseManager::instance())
{
    this->album = album;
    m_entries = dbm.imagedao.read_all_image();
//    qDebug() << m_entries[3]->description;
    //readAll() from database
}

int Image::rowCount(const QModelIndex &parent) const {
    return parent.isValid()? 0 : m_entries.count();
}

QVariant Image::data(const QModelIndex &index, int role) const {
    if (index.row() < 0 || index.row()>= m_entries.count())
        return QVariant();

    const Entry* entry = m_entries[index.row()];

    switch (role) {
    case LocationRole:
        return entry->location;
    case album_idRole:
        return entry->album_id;
    case sourceRole:
        qDebug() << "source is called";
        return entry->source;
    case descriptionRole:
        return entry->description;
    case datetimeRole:
        return entry->datetime;
    default:
        return QVariant();
    }
}

QHash<int, QByteArray> Image::roleNames() const{
    QHash<int, QByteArray> names;

    names.insert(LocationRole, "location");
    names.insert(album_idRole, "album_id");
    names.insert(sourceRole, "source");
    names.insert(descriptionRole, "description");
    names.insert(datetimeRole, "datetime");

    return names;
}

QVariantMap Image::get_data(int index) {
    qDebug() << "get_data called" ;
    Entry* entry = m_entries[index];

    QVariantMap mp;
    mp["location"] = entry->location;
    mp["description"] = entry->description;
    mp["album_id"] = entry->album_id;
    return mp;

}

int Image::get_index(QString source)
{
    for(int i=0; i<m_entries.count(); i++){
        if(m_entries[i]->source == source)
            return i;
    }

    return -1;
}

//QString Image::get_album_name(int id) {
//    return album.get
//}

Image::Entry* Image::varToentry(QVariant var, Entry* entry) {
    QVariantMap mp = var.toMap();

//    Entry entry;
    entry->location = mp["location"].toString();
    entry->source = mp["source"].toString();
    entry->description = mp["description"].toString();

    if(mp.contains("index")){
        int prevId = mp["album_id"].toInt();
//        entry->album_id = album->get_album_id(mp["index"].toInt());
        if(album->get_latest_image_source(prevId) == entry->source){
            QString new_latest_image_source = find_latest_image_source(prevId, entry->source);

            album->update_latest_image_source(prevId, new_latest_image_source);

            }
        entry->album_id = album->get_album_id(mp["index"].toInt());
        album->update_latest_image_source(entry->album_id, entry->source);

    }
    else {
        entry->album_id = mp["album_id"].toInt();
    }

    qDebug() << entry->description;

    return entry;
}

QString Image::find_latest_image_source(int id, QString last_source)
{
    bool flag = false;
    QDateTime datetime;
    int index = -1;
    for(int i = 0; i<m_entries.count(); i++){
        if(m_entries[i]->album_id == id && m_entries[i]->source != last_source){
            if(!flag) {
                datetime = m_entries[i]->datetime;
                index = i;
                flag = true;
            }
            else{
                if(datetime > m_entries[i]->datetime){
                    datetime = m_entries[i]->datetime;
                    index = i;
                }
            }
        }
    }
    if(index < 0)
        return "./assets/dummy-image-square.jpg";
    else
        return m_entries[index]->source;
}

QVector <Image::Entry*> Image::read_all_images() {
    return dbm.imagedao.read_all_image();
}

void Image::insert_data(QVariant var) {

    Entry* entry = new Entry();
    entry = varToentry(var, entry);
    entry->datetime = QDateTime::currentDateTime();

    album->update_latest_image_source(entry->album_id, entry->source);

    if(dbm.imagedao.insert_image(entry)){
    beginInsertRows(QModelIndex(), m_entries.count(), m_entries.count());
    m_entries.append(entry);
    endInsertRows();
    emit image_inserted();
    }
}

void Image::delete_data(QVariant var) {
    QString source = var.toString();
    int index = 0;

    for(int i = 0; i < m_entries.length(); i++){
        if(m_entries[i]->source == source) {
            index = i;
            break;
        }
    }
    if(dbm.imagedao.delete_image(source)){
    beginRemoveRows(QModelIndex(), index, index);
    m_entries.remove(index);
    endRemoveRows();
    }

}

void Image::update_data(QVariant var){
    Entry* entry = new Entry();
    entry = varToentry(var, entry);

    int index = 0;

    for(int i = 0; i < m_entries.length(); i++){
        if(m_entries[i]->source == entry->source) {
            index = i;
            break;
        }
    }


//    album->update_album(var);
    //check whether the image was latest image of the previous album
    if(dbm.imagedao.update_image(entry)){
    beginResetModel();
    qDebug() << entry->description;
    m_entries[index]->location = entry->location; m_entries[index]->album_id = entry->album_id; m_entries[index]->description = entry->description;
    endResetModel();
    }

    delete entry;
}

void Image::resize_image(QVariant var)
{
    qDebug() << "Image resized";
    QVariantMap mp = var.toMap();
    int width = mp["width"].toInt();
    int height = mp["height"].toInt();
    QString source = mp["source"].toString();

    int index = get_index(source);

    if(index < 0) return;

    QStringList splits = source.split("file:///");

    Mat img = imread(splits[1].toStdString());

    Mat resizedImg;

    resize(img, resizedImg, Size(width, height), INTER_LINEAR);

//    splits = splits[1].split(".");

//    String filename = splits[0].toStdString() + "_resized." + splits[1].toStdString();

//    qDebug() << filename.c_str();

    beginResetModel();
    imwrite(splits[1].toStdString(), resizedImg);
//    insert_data(make_qvariant(filename.c_str()));
    //QQmlApplicationEngine::clearComponentCache();
    endResetModel();

    qDebug() << "Image resized";

}

void Image::crop_image(QVariant var)
{
    QVariantMap mp = var.toMap();
    QString source = mp["source"].toString();

    int index = get_index(source);

    if(index < 0) return;

    QStringList splits = source.split("file:///");

    Mat img = imread(splits[1].toStdString());

    int width = img.size().width;
    int height = img.size().height;

    int y1 = mp["y1"].toDouble()*height;
    int y2 = mp["y2"].toDouble()*height;
    int x1 = mp["x1"].toDouble()*width;
    int x2 = mp["x2"].toDouble()*width;

    qDebug() << y1 << " " << y2 << x1 << " " << x2;

    qDebug() << img.size().width << " " << img.size().height ;

    Mat crop = img(Range(y1,y2),Range(x1,x2));

//    splits = splits[1].split(".");

//    String filename = splits[0].toStdString() + "_croped." + splits[1].toStdString();

//    qDebug() << filename.c_str();

    beginResetModel();
    imwrite(splits[1].toStdString(), crop);
//    insert_data(make_qvariant(filename.c_str()));
    endResetModel();
}


void Image::bright_image(QVariant var)
{
    QVariantMap mp = var.toMap();

    double brightness = mp["brightness"].toDouble();

    QString source = mp["source"].toString();

    int index = get_index(source);

    if(index < 0) return;

    QStringList splits = source.split("file:///");

    Mat img = imread(splits[1].toStdString());

    Mat changedImg;

    img.convertTo(changedImg, -1, 1, 256*brightness);

//    splits = splits[1].split(".");

//    String filename = splits[0].toStdString() + "_brighted." + splits[1].toStdString();

//    qDebug() << filename.c_str();

    if(imwrite(splits[1].toStdString(), changedImg)){
//        QCoreApplication::processEvents(QEventLoop::AllEvents, 1000);
        beginResetModel();
//        qmlengine->clearComponentCache();
//        qmlengine->trimComponentCache();
//        insert_data(make_qvariant(filename.c_str()));
        endResetModel();
//        emit image_edited();
    }
//    imwrite(splits[1].toStdString(), changedImg);
//    m_entries[index]->source = source;

}

void Image::sharp_image(QVariant var)
{
    QString source = var.toString();

    int index = get_index(source);

    if(index < 0) return;

    QStringList splits = source.split("file:///");

    Mat img = imread(splits[1].toStdString());

    Mat changedImg;

    Mat kernel2 = (Mat_<double>(3,3) << 0, -1, 0, -1, 5, -1, 0, -1, 0);

    filter2D(img, changedImg, -1 , kernel2, Point(-1, -1), 0, 4);

//    splits = splits[1].split(".");

//    String filename = splits[0].toStdString() + "_sharped." + splits[1].toStdString();

//    qDebug() << filename.c_str();

    beginResetModel();
    imwrite(splits[1].toStdString(), changedImg);
//    insert_data(make_qvariant(filename.c_str()));
    endResetModel();

}

QVariant Image::make_qvariant(QString filename)
{
    QMap<QString, QVariant> data;
    data["location"] = QVariant("");
    data["description"] = QVariant("");
    data["source"] = QVariant("file:///" + filename);
    data["album_id"] = 1;

    return QVariant(data);
}
