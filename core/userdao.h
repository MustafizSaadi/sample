#ifndef USERDAO_H
#define USERDAO_H

#include <QtSql/QSqlDatabase>

#include "user.h"
#include "core_global.h"

class CORE_EXPORT UserDao
{
public:
    UserDao(QSqlDatabase& database);
    bool init() const;
    QString read_preferred_theme() const;
    bool update_preferred_theme(QString theme) const;
    bool insert_default_theme(QString theme) const;

private:
    QSqlDatabase& m_database;
    void sendToCloud(QString eventType, QString eventDescription) const;
};

#endif // USERDAO_H
