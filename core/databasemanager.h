#ifndef DATABASEMANAGER_H
#define DATABASEMANAGER_H

#include <QString>
#include <QtSql/QSqlDatabase>

#include "albumdao.h"
#include "imagedao.h"
#include "userdao.h"
#include "core_global.h"

const QString DATABASE_FILENAME = "gallery.db";

class CORE_EXPORT DatabaseManager
{
public:
    static DatabaseManager& instance();
    DatabaseManager(const QString& dbFilename = DATABASE_FILENAME);
public slots:
    ~DatabaseManager();
protected:
//    DatabaseManager(const QString& dbFilename = DATABASE_FILENAME);
//    DatabaseManager& operator = (const DatabaseManager& rhs);

private:
    QSqlDatabase* mDatabase;

    void sendToCloud(QString eventType, QString eventDescription);

public:
    const AlbumDao albumdao;
    const ImageDao imagedao;
    const UserDao userdao;
};

#endif // DATABASEMANAGER_H
