#ifndef ALBUM_H
#define ALBUM_H

#include "core_global.h"
//#include "databasemanager.h"
//#include "albumdao.h"
//#include "image.h"

#include <QAbstractListModel>
#include <QtQml/QQmlEngine>
#include <QList>
#include <QDateTime>
#include <QStringListModel>

class DatabaseManager;

class CORE_EXPORT Album: public QAbstractListModel
{
    Q_OBJECT
public:
    enum Roles {
        idRole,nameRole, descriptionRole, latestImageSourceRole, datetimeRole
    };
    Album(QObject* parent = nullptr);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;

    QHash<int, QByteArray> roleNames() const override;

    Q_INVOKABLE int get_album_index(int id);
//    Q_INVOKABLE QStringListModel album_names();
    Q_INVOKABLE int get_album_id(int index);

    QString get_latest_image_source(int id);
    void set_latest_image_source(int id, QString source);


    struct Entry_album {
        int id;
        QString name;
        QString description;
        QDateTime datetime;
        QString latest_image_source;
//        Image *images;
    };

    QList<QVariant> album_names;

public slots:
    void update_latest_image_source(int id, QString source);
    QVector<Album::Entry_album*> read_all_albums();
    void insert_album(QVariant var);
    void delete_album(int id);
    void update_album(QVariant var);

//    void insert_images(QVariant var);
//    void delete_images(QVariant var);
//    void update_images(QVariant var);

private:
    Entry_album* varToentry(QVariant var, Entry_album* album);
    DatabaseManager& dbm;
//    const AlbumDao& albumdao;

protected:
    QVector <Entry_album*> m_entries;
};

#endif // ALBUM_H
