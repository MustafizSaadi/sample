#ifndef GOOGLEAUTH_H
#define GOOGLEAUTH_H

#include "core_global.h"

#include <QObject>
#include <QtNetworkAuth/QOAuth2AuthorizationCodeFlow>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QUrl>
#include <QUrlQuery>
#include <QtNetworkAuth/QOAuthHttpServerReplyHandler>
#include <QDesktopServices>


class CORE_EXPORT GoogleAuth : public QObject
{
    Q_OBJECT
public:
    explicit GoogleAuth(QObject *parent = nullptr, QThread *thread = nullptr);

//    QString getImage(QString fileId);

signals:
    void parse_JSON(QByteArray bytearray);
    void parsing_finished(QString fileId);
    void metadata_achieved(QString fileId);
    void image_achieved(QVariant var);


public slots:
    void click(QString filename);
//    void replyFinished(QNetworkReply *reply);
    void JSON_Parser(QByteArray bytearra);
    void getMetadata(QString fileId);
    void getImage(QString fileId);

private:
    QOAuth2AuthorizationCodeFlow * google;
    QThread *thread;
    QString filename;
    QString fileId;
    QByteArray bytearray;
    int width, height;
};

#endif // GOOGLEAUTH_H
