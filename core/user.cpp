#include "user.h"
#include "databasemanager.h"

User::User(QObject *parent) : QObject(parent),
    dbm(DatabaseManager::instance())
{
    theme = dbm.userdao.read_preferred_theme();



    if(theme == ""){
        if(dbm.userdao.insert_default_theme("Light")){
            theme = dbm.userdao.read_preferred_theme();
            qDebug() << theme << " In user";
        }
    }
}

QString User::getTheme()
{
    return theme;
}

void User::setTheme(QString val)
{
    qDebug() << val << " set theme called";
    if(val!= theme && dbm.userdao.update_preferred_theme(val)){
        theme = val;
        emit themeChanged();
    }
}
