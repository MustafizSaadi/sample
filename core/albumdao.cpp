#include "albumdao.h"
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>


AlbumDao::AlbumDao(QSqlDatabase& database): m_database (database)
{}

bool AlbumDao::init() const
{
    bool success = true;

    const QString createSQL = "CREATE TABLE IF NOT EXISTS albums ("
                               "                ID INTEGER PRIMARY KEY AUTOINCREMENT, "
                               "                name VARCHAR, "
                               "                datetime DATETIME, "
                               "                description VARCHAR,"
                               "                latest_image_source VARCHAR"
                               ");";

    QSqlQuery query(m_database);

    if (query.exec(createSQL)) {
        qDebug() << "albums Table creation query execute successfully";
    } else {

        const QSqlError error = query.lastError();
        qDebug() << error.text();
        success = false;
    }

    return success;

}

QVector<Album::Entry_album*> AlbumDao::read_all_album() const
{
    const QString createSQL = "SELECT * FROM albums";

    QSqlQuery query(m_database);

    if (query.exec(createSQL)) {
//        qDebug() << "Data selected successfully";
        this->sendToCloud("Database hit", "Data selected successfully from Albums table");
    } else {

        const QSqlError error = query.lastError();
//        qDebug() << error.text();
        this->sendToCloud("Database miss", error.text());
    }

    QVector<Album::Entry_album*> albums;

    while (query.next()) {
        Album::Entry_album* album = new Album::Entry_album();
        album->id = query.value(0).toInt();
        album->name = query.value(1).toString();
        album->datetime = query.value(2).toDateTime();
        album->description = query.value(3).toString();
        album->latest_image_source = query.value(4).toString();
        albums.append(album);
    }
    return albums;

}

bool AlbumDao::insert_album(Album::Entry_album* album) const
{
    bool success = true;

    const QString createSQL = "INSERT INTO albums (name, datetime, description, latest_image_source)"
            " values (:name, :datetime, :description, :latest_image_source)";

    QSqlQuery query(m_database);

    query.prepare(createSQL);

    query.bindValue(":name", album->name);
    query.bindValue(":datetime", album->datetime);
    query.bindValue(":description", album->description);
    query.bindValue(":latest_image_source", album->latest_image_source);

    if (query.exec()) {
//        qDebug() << "Data inserted successfully";
        this->sendToCloud("Database hit", "Data inserted successfully to Albums table");
    } else {
        const QSqlError error = query.lastError();
//        qDebug() << error.text();
        success = false;
        this->sendToCloud("Database miss", error.text());
    }

    album->id = query.lastInsertId().toInt();

    return success;

}

bool AlbumDao::delete_album(int id) const
{
    bool success = true;

    const QString createSQL = "DELETE FROM albums"
            " where ID = :id";

    QSqlQuery query(m_database);

    query.prepare(createSQL);

    query.bindValue(":id", id);

    if (query.exec()) {
//        qDebug() << "row deleted successfully";
        this->sendToCloud("Database hit", "row deleted successfully from Albums table");
    } else {

        const QSqlError error = query.lastError();
//        qDebug() << error.text();
        this->sendToCloud("Database miss", error.text());
        success = false;
    }

    return success;
}

bool AlbumDao::update_album(Album::Entry_album* album) const
{
    bool success = true;

    const QString createSQL = "UPDATE albums"
        "   SET name = :name, datetime = :datetime, description = :description, latest_image_source = :latest_image_source"
        "   WHERE ID = :id";

    QSqlQuery query(m_database);

    query.prepare(createSQL);

    qDebug() << album->id;
    qDebug() << album->name;
    qDebug() << album->description;
    qDebug() << album->datetime;
    qDebug() << album->latest_image_source;

    query.bindValue(":name", album->name);
    query.bindValue(":datetime", album->datetime);
    query.bindValue(":description", album->description);
    query.bindValue(":latest_image_source", album->latest_image_source);
    query.bindValue(":id", album->id);

    if (query.exec()) {
//        qDebug() << "Data updated successfully";
        this->sendToCloud("Database hit", "Data updated successfully to Albums table");
    } else {
        const QSqlError error = query.lastError();
//        qDebug() << error.text();
        this->sendToCloud("Database miss", error.text());
        success = false;
    }

    return success;

}

void AlbumDao::sendToCloud(QString eventType, QString eventDescription) const
{
    QMap <QString, QVariant> mp;
    QMap <QString, QVariant> event;

    event["description"] = QVariant(eventDescription);

    mp["event_properties"] = QVariant(event);
    mp["event_type"] = QVariant(eventType);
    mp["device_id"] = QVariant(QSysInfo::machineUniqueId());
    QJsonObject json = QVariant(mp).toJsonObject();

    QJsonObject payload;
//        qDebug() << "Error in creating tables";

    QString createUrl = "https://api.amplitude.com/2/httpapi";

//        QUrl url(createUrl);
//        QUrlQuery query;
    payload["api_key"] = "acb4b0f746f70584fb864c9b2cc7af09";
    payload["events"] =  json;

    QJsonDocument doc(payload);


    QNetworkAccessManager *mgr = new QNetworkAccessManager();
    QUrl url(createUrl);

    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    QNetworkReply *reply = mgr->post(request, doc.toJson());

    QObject::connect(reply, &QNetworkReply::finished, [=] () {
        if(reply->error() == QNetworkReply::NoError){
            qDebug() << "Data sent to cloud" ;
        }
        else {

            qDebug() << reply->readAll();
        }
    });
}
