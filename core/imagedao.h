#ifndef IMAGEDAO_H
#define IMAGEDAO_H

#include <QtSql/QSqlDatabase>

#include "image.h"
#include "core_global.h"

class CORE_EXPORT ImageDao
{
public:
    ImageDao(QSqlDatabase& database);
    bool init() const;
    QVector <Image::Entry*> read_all_image() const;
    bool insert_image(Image::Entry* image) const;
    bool delete_image(QString source) const;
    bool update_image(Image::Entry* image) const;
    bool add_column() const;

private:
    QSqlDatabase& m_database;
    void sendToCloud(QString eventType, QString eventDescription) const;

};

#endif // IMAGEDAO_H
