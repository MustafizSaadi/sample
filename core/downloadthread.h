#ifndef DOWNLOADTHREAD_H
#define DOWNLOADTHREAD_H

#include<QThread>

#include "googleauth.h"
#include "image.h"
#include "core_global.h"

class CORE_EXPORT DownloadThread: public QThread
{
    Q_OBJECT
public:
    explicit DownloadThread(QObject *parent = nullptr, Image *image=nullptr);
//    ~DownloadThread();

public slots:
    void starter(QVariant var);
    void end();

protected:
    void run() override;

private:
    QString filename;
//    GoogleAuth *googleauth;
    Image *image;
    void sendToCloud(QString eventType, QString eventDescription) const;

};

#endif // DOWNLOADTHREAD_H
