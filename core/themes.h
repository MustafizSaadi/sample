#ifndef THEMES_H
#define THEMES_H

#include <QAbstractListModel>

#include "core_global.h"

class CORE_EXPORT Themes: public QAbstractListModel
{
    Q_OBJECT
public:
    enum Roles { textRole, sourceRole };
    explicit Themes(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;

    QHash<int, QByteArray> roleNames() const override;

    struct theme{
        QString name;
        QString imageSource;
    };

    QVector <theme> themes;
};

#endif // THEMES_H
