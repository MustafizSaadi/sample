#include "imagedatetimesortproxymodel.h"
#include "image.h"

ImageDatetimeSortProxyModel::ImageDatetimeSortProxyModel(QObject *parent)
    : QSortFilterProxyModel(parent)
{
    sort(0);
    //setDynamicSortFilter(true);
}

bool ImageDatetimeSortProxyModel::lessThan(const QModelIndex &sourceLeft, const QModelIndex &sourceRight) const
{
    const QDateTime leftData = sourceLeft.data(Image::datetimeRole).toDateTime();
    const QDateTime rightData = sourceRight.data(Image::datetimeRole).toDateTime();
    //bool flag = leftData < rightData;
    //qInfo() << leftData << " " << rightData ;
    return leftData > rightData;
}
