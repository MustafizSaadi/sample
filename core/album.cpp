#include "album.h"
#include "databasemanager.h"
#include <QMap>

Album::Album(QObject *parent) : QAbstractListModel(parent),
    dbm(DatabaseManager::instance())
{

    m_entries = dbm.albumdao.read_all_album();

    if(m_entries.count() == 0) {
        QMap <QString, QVariant> mp;
        mp["name"] = QVariant("Default");
        mp["description"] = QVariant("Default album for all the images that are not assigned to any album.");
        //        album.datetime = QDateTime::currentDateTime();
        insert_album(QVariant(mp));

    }
    else {
        for(int i=0; i< m_entries.count(); i++){
            album_names.append(QVariant(m_entries[i]->name));
        }
    }

}

int Album::rowCount(const QModelIndex &parent) const {
    return parent.isValid()? 0 : m_entries.count();
}

QVariant Album::data(const QModelIndex &index, int role) const {
    if (index.row() < 0 || index.row()>= m_entries.count())
        return QVariant();

    const Entry_album *entry = m_entries[index.row()];

    switch (role) {
    case idRole:
        return entry->id;
    case nameRole:
        return entry->name;
    case descriptionRole:
        return entry->description;
    case latestImageSourceRole:
        return entry->latest_image_source;
    case datetimeRole:
        return entry->datetime;
    default:
        return QVariant();
    }
}

QHash<int, QByteArray> Album::roleNames() const{
    QHash<int, QByteArray> names;

    names.insert(nameRole, "name");
    names.insert(descriptionRole, "description");
    names.insert(latestImageSourceRole, "latestImageSource");
    names.insert(idRole, "id");
    names.insert(datetimeRole, "datetime");

    return names;
}

//QStringListModel Album::album_names() {
//    QStringListModel model;
//    QStringList names;
//    for(int i = 0; i<m_entries.count(); i++)
//        names.append(m_entries[i]->name);
//    qDebug() << names.length();
//    model.setStringList(names);
//    QQmlEngine::setObjectOwnership(&model, QQmlEngine::CppOwnership);
//    return model;
//}

int Album::get_album_index(int id) {
    for(int i = 0; i<m_entries.count(); i++)
        if(m_entries[i]->id == id) return i;

    return -1;
}

int Album::get_album_id(int index) {
    return m_entries[index]->id;
}

QVector<Album::Entry_album*> Album::read_all_albums()
{
    return dbm.albumdao.read_all_album();
}

QString Album::get_latest_image_source(int id)
{
    for(int i = 0; i<m_entries.count(); i++)
        if(m_entries[i]->id == id) return m_entries[i]->latest_image_source;
    return "No source Found";
}

Album::Entry_album* Album::varToentry(QVariant var, Entry_album* entry) {
    QVariantMap mp = var.toMap();

    entry->name = mp["name"].toString();
    entry->description = mp["description"].toString();
    entry->id = mp["id"].toInt();

    //    qDebug() << entry.location;

    return entry;
}

void Album::insert_album(QVariant var) {
    Entry_album* entry = new Entry_album();
    entry = varToentry(var, entry);
    //    Image* images = new Image();

    //    entry.images = images;
    entry->datetime = QDateTime::currentDateTime();
    entry->latest_image_source = "./assets/dummy-image-square.jpg";
    if(dbm.albumdao.insert_album(entry)){
        beginInsertRows(QModelIndex(), m_entries.count(), m_entries.count());
        m_entries.append(entry);
        endInsertRows();

        //        namemodel->insertRows(album_names.count(), 1);
        album_names.append(entry->name);
    }
}

void Album::delete_album(int id) {

    int index = 0;

    for(int i = 0; i < m_entries.length(); i++){
        if(m_entries[i]->id == id) {
            index = i;
            break;
        }
    }
    if(index!=0 && dbm.albumdao.delete_album(id)){
        beginRemoveRows(QModelIndex(), index, index);
        m_entries.remove(index);
        endRemoveRows();

        //    beginRemoveRows(QModelIndex(), index, index);
        album_names.removeAt(index);
        //    endRemoveRows();
    }
    else if(index == 0){
        qDebug() << "default album deletion operation attempted";
    }

}


void Album::update_album(QVariant var){
    Entry_album* entry = new Entry_album();
    entry = varToentry(var, entry);

    int index = 0;

    for(int i = 0; i < m_entries.length(); i++){
        if(m_entries[i]->id == entry->id) {
            index = i;
            break;
        }
    }

    if(dbm.albumdao.update_album(entry)){
        beginResetModel();
        m_entries[index]->name = entry->name; m_entries[index]->description = entry->description;
        endResetModel();
    }
    delete entry;

}

void Album::update_latest_image_source(int id, QString source){
    int index = 0;

    for(int i = 0; i < m_entries.length(); i++){
        if(m_entries[i]->id == id) {
            index = i;
            break;
        }
    }
    Entry_album* entry;
    entry = m_entries[index];
    entry->latest_image_source = source;
    //    entry->datetime = QDateTime::currentDateTime();
    if(dbm.albumdao.update_album(entry)){
        beginResetModel();
        qDebug() << "data updated";
        m_entries[index]->latest_image_source = entry->latest_image_source;
        //    m_entries[index]->datetime = entry->datetime;
        endResetModel();
    }
    //    delete entry;

}
