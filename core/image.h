#ifndef IMAGE_H
#define IMAGE_H
#include <QAbstractListModel>
#include <QtQml/QQmlApplicationEngine>
#include <QDateTime>
#include <QStack>

#include "core_global.h"
#include "album.h"
//#include "databasemanager.h"
//#include "imagedao.h"

class DatabaseManager;

class CORE_EXPORT Image: public QAbstractListModel
{
    Q_OBJECT
public:
    enum Roles {
        LocationRole, album_idRole, sourceRole, descriptionRole, datetimeRole
    };

    explicit Image(QObject* parent = nullptr, Album *album = nullptr);

    Album *album;
    QQmlApplicationEngine *qmlengine;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;

    QHash<int, QByteArray> roleNames() const override;

    struct Entry {
        QString location;
        int album_id;
        QString source;
        QString description;
        QDateTime datetime;
    };

    Q_INVOKABLE QVariantMap get_data(int index);
    int get_index(QString source);
    QString find_latest_image_source(int id, QString last_source);
//    Q_INVOKABLE QString get_album_name(int id);

    Entry* varToentry(QVariant var, Entry* image);
signals:
    void image_edited();
    void image_inserted();

public slots:
    QVector <Image::Entry*> read_all_images();
    void insert_data(QVariant var);
    void delete_data(QVariant var);
    void update_data(QVariant var);
    void resize_image(QVariant var);
    void crop_image(QVariant var);
    void bright_image(QVariant var);
    void sharp_image(QVariant var);
    QVariant make_qvariant(QString filename);


private:
//    Entry varToentry(QVariant var);
    DatabaseManager& dbm;

protected:

    QVector <Entry*> m_entries;

};

#endif // IMAGE_H
