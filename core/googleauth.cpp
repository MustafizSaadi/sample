#include "googleauth.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QImage>
#include <QFile>
#include <QPixmap>
#include <QDir>
#include <QtCore/QThread>

GoogleAuth::GoogleAuth(QObject *parent, QThread *thread):QObject(parent)
{
    this->google = new QOAuth2AuthorizationCodeFlow(this);
    this->thread = thread;

    this->google->setScope("email https://www.googleapis.com/auth/drive.readonly");

//    QUrl url("https://www.googleapis.com/auth/drive.readonly");
//    QUrlQuery query;
//    query.addQueryItem("prompt", "consent");
//    query.addQueryItem("access_type", "offline");
//    url.setQuery(query.query());

    QObject::connect(this, SIGNAL(parse_JSON(QByteArray)), this, SLOT(JSON_Parser(QByteArray)));
    QObject::connect(this, SIGNAL(parsing_finished(QString)), this, SLOT(getMetadata(QString)));
    QObject::connect(this, SIGNAL(metadata_achieved(QString)), this, SLOT(getImage(QString)));

    connect(this->google, &QOAuth2AuthorizationCodeFlow::authorizeWithBrowser, [=](QUrl url) {
        QUrlQuery query(url);

        query.addQueryItem("prompt", "consent");      // Param required to get data everytime
        query.addQueryItem("access_type", "offline"); // Needed for Refresh Token (as AccessToken expires shortly)
        url.setQuery(query);
        QDesktopServices::openUrl(url);
    });

    this->google->setAuthorizationUrl(QUrl("https://accounts.google.com/o/oauth2/auth"));
    this->google->setAccessTokenUrl(QUrl("https://oauth2.googleapis.com/token"));
    this->google->setClientIdentifier("160193811701-qvi3c60nt1hk04cg1ebujqk9efr5pk7b.apps.googleusercontent.com");
    this->google->setClientIdentifierSharedKey("XPGJFN-sYnMgE1RupiUWP13h");

    auto replyHandler = new QOAuthHttpServerReplyHandler(5470, this);
    this->google->setReplyHandler(replyHandler);

    QObject::connect(this->google, &QOAuth2AuthorizationCodeFlow::granted, [=]() {
        qDebug() << __FUNCTION__ << __LINE__ << "Access Granted!";
        QUrl url("https://www.googleapis.com/drive/v3/files");
        //    QUrlQuery query;
        //    query.addQueryItem("alt", "media");
        //    url.setQuery(query.query());
        auto reply = this->google->get(url);
//        QNetworkAccessManager *manager = new QNetworkAccessManager();

        QObject::connect(reply, &QNetworkReply::finished, [=] (){
            bytearray = reply->readAll();
            emit parse_JSON(bytearray);
    });

//        manager->get(QNetworkRequest(url));
    });


}

void GoogleAuth::JSON_Parser(QByteArray bytearray) {
//    QByteArray bytearray = reply->readAll();

    qDebug() << bytearray;

    QJsonDocument doc = QJsonDocument::fromJson(bytearray);

    QJsonObject jObject = doc.object();

    QJsonObject::iterator it_data = jObject.find("files");

    QJsonArray array = it_data.value().toArray();

//            QJsonObject jObject = doc.object();

   QJsonArray::iterator it;

   for(it = array.begin(); it!=array.end(); it++) {
       QJsonObject tuple = (*it).toObject();

       if(tuple.find("name").value().toString() == this->filename) {
           fileId = tuple.find("id").value().toString();
           break;
       }

   }

   emit parsing_finished(fileId);


//   QString source = getImage(fileId);

}

void GoogleAuth::getMetadata(QString fileId)
{
    QString createUrl = "https://www.googleapis.com/drive/v3/files/" + fileId;
    QUrl url(createUrl);
    QUrlQuery query;
    query.addQueryItem("fields", "imageMediaMetadata");
    url.setQuery(query.query());
    auto metadata_reply = this->google->get(url);

    QObject::connect(metadata_reply, &QNetworkReply::finished, [=] () {
        QByteArray bytearray = metadata_reply->readAll();

//        qDebug() << bytearray;

        QJsonDocument doc = QJsonDocument::fromJson(bytearray);

        QJsonObject object = doc.object();

        QJsonObject::iterator it_data = object.find("imageMediaMetadata");

        QJsonObject tuple = (*it_data).toObject();

        width = tuple.find("width").value().toInt();
        height = tuple.find("height").value().toInt();

//        QFile f("response.json");
//        if(f.open(QIODevice::WriteOnly))
//        {
//            QDataStream out(&f);
//            out << doc;
//                f.close();
//        }

        qDebug() << width << " " << height;
    });

    emit metadata_achieved(fileId);
}

void GoogleAuth::getImage(QString fileId)
{
    QString createUrl = "https://www.googleapis.com/drive/v3/files/" + fileId;
    QUrl url(createUrl);
    QUrlQuery query;
    query.addQueryItem("alt", "media");
    url.setQuery(query.query());
    auto content_reply = this->google->get(url);

    QObject::connect(content_reply, &QNetworkReply::finished, [=] (){
        bytearray = content_reply->readAll();

        QPixmap p;
//        QByteArray pData;
        // fill array with image
        if(p.loadFromData(bytearray,"JPEG"))
        {
           // do something with pixmap
            QFile file(filename);
            file.open(QIODevice::WriteOnly);
            qDebug() << p.save(&file, "JPEG");
        }

        QMap <QString, QVariant> mp;

        mp["location"] = "";
        mp["description"] = "";
        mp["album_id"] = 1;
        mp["source"] = "file:///" + QDir::currentPath() + "/"+ filename;

        emit image_achieved(QVariant(mp));

        this->thread->exit();
//        this->thread->finished();



//        qDebug() << mp["source"];


//        QImage image (reinterpret_cast<const unsigned char *>(bytearray.data()), width, height, QImage::Format_RGBX64);
//        qDebug() << bytearray.length();
////        image.save(filename, "png");
//        qDebug() << image.save(filename, "png");


    });

}

void GoogleAuth::click(QString filename)
{
    this->filename = filename;
    this->google->grant();

//    QThread *thread = QThread::create(getImage(""));

//    JSON_Parser();

//    qDebug() << fileId << " ?? ";

//    if(fileId.length() != 0){
//        QString source = getImage(fileId);
//    }



}
