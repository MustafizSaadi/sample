#include "imagedao.h"
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>

ImageDao::ImageDao(QSqlDatabase& database): m_database(database)
{}

bool ImageDao::init() const
{
    bool success = true;

    const QString createSQL = "CREATE TABLE IF NOT EXISTS images ("
                               "                ID INTEGER PRIMARY KEY AUTOINCREMENT,"
                               "                location VARCHAR,"
                               "                albumID INTEGER,"
                               "                source VARCHAR,"
                               "                description VARCHAR,"
                               "                datetime DATETIME,"
                               "                FOREIGN KEY (albumID) REFERENCES albums(ID)"
                               ");";

    QSqlQuery query(m_database);

    if (query.exec(createSQL)) {
        qDebug() << "images Table creation query execute successfully";
    } else {

        const QSqlError error = query.lastError();
        qDebug() << error.text();
        success = false;
    }

    return success;

}

QVector <Image::Entry*> ImageDao::read_all_image() const
{
    const QString createSQL = "SELECT * FROM images";

    QSqlQuery query(m_database);

    if (query.exec(createSQL)) {
//        qDebug() << "Data selected successfully";
        this->sendToCloud("Database hit", "Data selected successfully from images table");
    } else {

        const QSqlError error = query.lastError();
//        qDebug() << error.text();
        this->sendToCloud("Database miss", error.text());
    }

    QVector<Image::Entry*> images;

    while (query.next()) {
        Image::Entry* image = new Image::Entry();

        image->location = query.value(1).toString();
        image->album_id = query.value(2).toInt();
        image->source = query.value(3).toString();
        image->description = query.value(4).toString();
        qDebug() << image->description;
        image->datetime = query.value(5).toDateTime();
        images.append(image);
    }
    return images;
}

bool ImageDao::insert_image(Image::Entry* image) const
{
    bool success = true;

    qDebug() << "insertion";

    const QString createSQL = "INSERT INTO images (location, albumID, source, description, datetime)"
            " values (:location, :album_id, :source, :description, :datetime)";

    QSqlQuery query(m_database);

    query.prepare(createSQL);

    query.bindValue(":location", image->location);
    query.bindValue(":album_id", image->album_id);
    query.bindValue(":source", image->source);
    query.bindValue(":description", image->description);
    query.bindValue(":datetime", image->datetime);

    if (query.exec()) {
//        qDebug() << "Data inserted successfully";
        this->sendToCloud("Database hit", "Data inserted successfully to images table");
    } else {
        const QSqlError error = query.lastError();
//        qDebug() << error.text();
        this->sendToCloud("Database miss", error.text());
        success = false;
    }

   return success;

}

bool ImageDao::delete_image(QString source) const
{
    bool success = true;

    const QString createSQL = "DELETE FROM images"
            " where source = :source";

    QSqlQuery query(m_database);

    query.prepare(createSQL);

    query.bindValue(":source", source);

    if (query.exec()) {
//        qDebug() << "row deleted successfully";
        this->sendToCloud("Database hit", "row deleted successfully from images table");
    } else {

        const QSqlError error = query.lastError();
//        qDebug() << error.text();
        this->sendToCloud("Database miss", error.text());
        success = false;
    }

    return success;
}

bool ImageDao::update_image(Image::Entry* image) const
{
    bool success = true;

    const QString createSQL = "UPDATE images"
        "   SET location = :location, albumID = :album_id, description = :description, datetime = :datetime"
        "   WHERE source = :source";

    QSqlQuery query(m_database);

    query.prepare(createSQL);

    query.bindValue(":location", image->location);
    query.bindValue(":album_id", image->album_id);
    query.bindValue(":description", image->description);
    query.bindValue(":datetime", image->datetime);
    query.bindValue(":source", image->source);

    if (query.exec()) {
        qDebug() << "Data updated successfully";
        this->sendToCloud("Database hit", "Data updated successfully to images table");
    } else {
        const QSqlError error = query.lastError();
//        qDebug() << error.text();
        this->sendToCloud("Database miss", error.text());
        success = false;
    }

    return success;
}

bool ImageDao::add_column() const
{
    bool success = true;

    const QString createSQL = "ALTER TABLE images"
            "                  ADD date DATE";

    QSqlQuery query(m_database);

    if (query.exec(createSQL)) {
//        qDebug() << "images Alter Table query execute successfully";
    } else {

        const QSqlError error = query.lastError();
//        qDebug() << error.text();
        success = false;
    }

    return success;
}

void ImageDao::sendToCloud(QString eventType, QString eventDescription) const
{
    QMap <QString, QVariant> mp;
    QMap <QString, QVariant> event;

    event["description"] = QVariant(eventDescription);

    mp["event_properties"] = QVariant(event);
    mp["event_type"] = QVariant(eventType);
    mp["device_id"] = QVariant(QSysInfo::machineUniqueId());
    QJsonObject json = QVariant(mp).toJsonObject();

    QJsonObject payload;
//        qDebug() << "Error in creating tables";

    QString createUrl = "https://api.amplitude.com/2/httpapi";

//        QUrl url(createUrl);
//        QUrlQuery query;
    payload["api_key"] = "acb4b0f746f70584fb864c9b2cc7af09";
    payload["events"] =  json;

    QJsonDocument doc(payload);


    QNetworkAccessManager *mgr = new QNetworkAccessManager();
    QUrl url(createUrl);

    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    QNetworkReply *reply = mgr->post(request, doc.toJson());

    QObject::connect(reply, &QNetworkReply::finished, [=] () {
        if(reply->error() == QNetworkReply::NoError){
            qDebug() << "Data sent to cloud" ;
        }
        else {

            qDebug() << reply->readAll();
        }
    });

}
