#include "albumdatetimeproxymodel.h"
#include "album.h"

AlbumDatetimeProxyModel::AlbumDatetimeProxyModel(QObject *parent): QSortFilterProxyModel(parent)
{
    sort(0);
}

bool AlbumDatetimeProxyModel::lessThan(const QModelIndex &sourceLeft, const QModelIndex &sourceRight) const
{
    const QDateTime leftData = sourceLeft.data(Album::datetimeRole).toDateTime();
    const QDateTime rightData = sourceRight.data(Album::datetimeRole).toDateTime();
    //bool flag = leftData < rightData;
    //qInfo() << leftData << " " << rightData ;
    return leftData > rightData;
}

int AlbumDatetimeProxyModel::getSourceId(int ind)
{
        QModelIndex proxyIndex = index(ind, 0, QModelIndex());
        return mapToSource(proxyIndex).data(Album::idRole).toInt();
}
