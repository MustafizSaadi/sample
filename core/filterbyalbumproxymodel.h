#ifndef FILTERBYALBUMPROXYMODEL_H
#define FILTERBYALBUMPROXYMODEL_H

#include <QSortFilterProxyModel>
#include <QModelIndex>

#include "core_global.h"

class CORE_EXPORT FilterbyAlbumProxyModel: public QSortFilterProxyModel
{
    Q_OBJECT

    Q_PROPERTY(int album_id READ album_id WRITE setAlbum_id NOTIFY album_idChanged)

public:
    explicit FilterbyAlbumProxyModel(QObject *parent = nullptr);

//    Q_INVOKABLE int get_source_index(int index);

    int album_id() const;

public slots:
    void setAlbum_id(int index);

signals:
    void album_idChanged();

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;

private:
    int m_album_id;
};

#endif // FILTERBYALBUMPROXYMODEL_H
