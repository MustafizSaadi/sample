TEMPLATE = subdirs

SUBDIRS += \
    core \
    desktop \
    testing

desktop.depends = core
testing.depends = core
